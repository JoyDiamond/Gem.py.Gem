#
#   Copyright (c) 2017-2018 Joy Diamond.  All rights reserved.
#
@module('Capital.SimpleStringIO')
def module():
    require_module('Capital.Import')


    if is_python_2:
        StringIO = import_module('cStringIO').StringIO
    else:
        StringIO = import_module('_io').StringIO


    @export
    def create_SimpleStringOutput():
        return StringIO()
