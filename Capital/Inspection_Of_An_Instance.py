#
#   Copyright (c) 2017-2018 Joy Diamond.  All rights reserved.
#
@module('Capital.Inspection_Of_An_Instance')
def module():
    class Inspection_Of_An_Instance(Object):
        __slots__ = ((
            'meta',                   #   Type+
            'class_name',             #   String+
        ))


        def __repr__(t):
            return arrange('<Inspection_Of_An_Instance %r>', t.class_name)


    new__Inspection_Of_An_Instance                    = Method(new_instance, Inspection_Of_An_Instance)
    initialize__Inspection_Of_An_Instance__meta       = Inspection_Of_An_Instance.meta      .__set__
    initialize__Inspection_Of_An_Instance__class_name = Inspection_Of_An_Instance.class_name.__set__


    @share
    def create__Inspection_Of_An_Instance(meta):
        class_name = python_class_name(meta)

        r = new__Inspection_Of_An_Instance()

        initialize__Inspection_Of_An_Instance__meta      (r, meta)
        initialize__Inspection_Of_An_Instance__class_name(r, class_name)

        return r
