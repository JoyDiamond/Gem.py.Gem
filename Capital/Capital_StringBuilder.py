#
#   Copyright (c) 2017-2018 Joy Diamond.  All rights reserved.
#
@module('Capital.Capital_StringBuilder')
def module():
    require_module('Capital.SimpleStringIO')                            #   create_SimpleStringOutput
    require_module('Capital.UniqueName')                                #   create_UniqueName


    Capital_StringBuilder__next_debug_name = create_UniqueName('sb').next


    class Capital_StringBuilder(Object):
        __slots__ = ((
            'debug_name',               #   None | String+
            'z',                        #   Zone
            'client',                   #   StringOutput
            'write',                    #   Method
        ))


        def __init__(t, z, client):
            t.debug_name = none
            t.z          = z
            t.client     = client
            t.write      = client.write


        def __repr__(t):
            debug_name = t.debug_name

            if debug_name is none:
                debug_name = \
                    t.debug_name = Capital_StringBuilder__next_debug_name()

            return arrange('<StringBuilder #%s @%s>', debug_name, t.z.debug_name)


        def finish_and_recycle(t):
            client = t.client

            assert client is not none

            t.write = \
                t.client = none

            r = client.getvalue()

            client.close()

            t.z.recycle__StringBuilder__ALLY__Capital_StringBuilder(t)

            return r


        def recycle__ALLY__Zone(t):
            assert t.client is t.write is none

            t.client = \
                client = create_SimpleStringOutput()

            t.write = client.write

            return t


        def write_2(t, a, b):
            write = t.write

            write(a)
            write(b)


        def write_3(t, a, b, c):
            write = t.write

            write(a)
            write(b)
            write(c)


    @export
    def create_StringBuilder__ALLY__Zone(z):
        return Capital_StringBuilder(z, create_SimpleStringOutput())
