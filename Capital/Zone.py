#
#   Copyright (c) 2017-2018 Joy Diamond.  All rights reserved.
#
@module('Capital.Zone')
def module():
    require_module('Capital.Exception')                                 #   raise_runtime_error
    require_module('Capital.UniqueName')                                #   create_UniqueName
    require_module('Capital.Capital_StringBuilder')                     #   create_StringBuilder__ALLY__Zone


    STRING_BUILDER_MAXIMUM = 10


    Zone__next_debug_name = create_UniqueName('z').next


    class Zone(Object):
        __slots__ = ((
            'debug_name',               #   String
            'zone_thread_number',       #   Integer
            'string_builder_append',    #   Method
            'string_builder_length',    #   Method
            'string_builder_pop',       #   Method
        ))


        first_thread_number = none
        first_zone          = none


        def __init__(z, debug_name, zone_thread_number, string_builder_many):
            z.debug_name            = debug_name
            z.zone_thread_number    = zone_thread_number
            z.string_builder_append = string_builder_many.append
            z.string_builder_length = string_builder_many.__len__
            z.string_builder_pop    = string_builder_many.pop


        def __repr__(z):
            return arrange('<Zone #%s>', z.debug_name)


        def summon_StringBuilder(z):
            if z.string_builder_length() > 0:
                return z.string_builder_pop().recycle__ALLY__Zone()

            return create_StringBuilder__ALLY__Zone(z)


        def recycle__StringBuilder__ALLY__Capital_StringBuilder(z, builder):
            if z.string_builder_length() < STRING_BUILDER_MAXIMUM:
                z.string_builder_append(builder)


    def create_Zone(zone_thread_number):
        return Zone(Zone__next_debug_name(), zone_thread_number, [])


    @export
    def current_zone():
        thread_number = thread_identifier()

        if thread_number == Zone.first_thread_number:
            return Zone.first_zone

        if Zone.first_thread_number is not none:
            raise_runtime_error('current_zone: only single threaded currently supported')

        zone = create_Zone(thread_number)

        Zone.first_thread_number = thread_number
        Zone.first_zone          = zone

        return zone
