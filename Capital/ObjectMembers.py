#
#  Copyright (c) 2018 Joy Diamond.  All rights reserved.
#
@module('Capital.ObjectMembers')
def module():
    show = 0


    require_module('Capital.Class_FindSymbol')


    #
    #   NOTE:
    #       See "LearningPython/ObjectParts.py" for more details.
    #
    find_Object_member = produce__find_symbol(Object, show = show)


    #
    #   All the members of `Object`
    #
    Object__member__class_type         = find_Object_member('__class__')
    Object__operator__delete_attribute = find_Object_member('__delattr__')
   #Object__member__documentation      = find_Object_member('__doc__')
   #Object__operator__format           = find_Object_member('__format__')
    Object__operator__get_attribute    = find_Object_member('__getattribute__')
   #Object__operator__hash             = find_Object_member('__hash__')
    Object__operator__constructor      = find_Object_member('__init__')
    Object__operator__new              = find_Object_member('__new__')
   #Object__operator__reduce           = find_Object_member('__reduce__')
   #Object__operator__reduce_extended  = find_Object_member('__reduce_ex__')
    Object__operator__representation   = find_Object_member('__repr__')
    Object__operator__set_attribute    = find_Object_member('__setattr__')
    Object__operator__sizeof           = find_Object_member('__sizeof__')
    Object__operator__string           = find_Object_member('__str__')
   #Object__operator__subclass_hook    = find_Object_member('__subclasshook__')


    if is_python_3:
        Object__operator__introspection = find_Object_member('__dir__')


    #
    #   Comparison operators
    #
   #if is_python_3:
   #    Object__operator__equal                 = find_Object_member('__eq__')
   #    Object__operator__greater_than          = find_Object_member('__gt__')
   #    Object__operator__greater_than_or_equal = find_Object_member('__ge__')
   #    Object__operator__less_than             = find_Object_member('__lt__')
   #    Object__operator__less_than_or_equal    = find_Object_member('__le__')
   #    Object__operator__not_equal             = find_Object_member('__ne__')



    #
    #   new_instance
    #
    #       Alias for `Object.__new__`, which allocates new vacant objects.
    #
    #       By "vacant" this means all the slots are unitialized (and return `AttributeError` if an attempt is made to
    #       access them).
    #
    new_instance = find_Object_member('__new__')


    #
    #   share & export
    #
    share(
            'Object__member__class_type',           Object__member__class_type,
            'Object__operator__constructor',        Object__operator__constructor,
            'Object__operator__get_attribute',      Object__operator__get_attribute,
            'Object__operator__representation',     Object__operator__representation,
            'Object__operator__sizeof',             Object__operator__sizeof,
            'Object__operator__string',             Object__operator__string,
        )


    if is_python_3:
        share(
                'Object__operator__introspection',  Object__operator__introspection,
            )


    export(
            'new_instance',                         new_instance,
            'Object__operator__new',                Object__operator__new,
            'Object__operator__set_attribute',      Object__operator__set_attribute,
            'Object__operator__delete_attribute',   Object__operator__delete_attribute,
        )
