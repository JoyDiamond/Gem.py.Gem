#
#   Copyright (c) 2017-2018 Joy Diamond.  All rights reserved.
#
@module('Capital.PropertyInspect')
def module():
    #
    #    NOTE:
    #        See below for `find_python_proxy_map_item` in a comment
    #
    MapProxy                   = type(Type.__dict__)
    find_python_proxy_map_item = MapProxy.__getitem__

    #
    #   The only reason this is a property is so that `M.inspect` will mean `type(M)::inspect.__get__(M)` which
    #   will return to `M::inspect__result`.
    #
    #       In other words, override `M.inspect` to *NOT* mean `M::inspect` but instead `M::_insect`
    #
    #   Example:
    #
    #       1.  `green`          is an instance of class `Color`
    #       2.  `Color`          uses metaclass `MetaColor`
    #       3.  `MetaColor`      uses metaclass `Meta_Metaclass`
    #       4.  `Meta_Metaclass` is it's own metaclass.
    #
    #   The following will exist:
    #
    #       1.  `green::inspect`                    -- Does *NOT* exist.
    #
    #       2A  `Color::inspect`                    -- The inspection of an instance `Color`
    #       2B  `Color::inspect__result`                -- The inspection of       class `Color`
    #
    #       3A. `Metacolor::inspect`                -- This will be set to `property_inspect`
    #       3B. `Metacolor::inspect`                -- The inspection of metaclass `Metacolor`
    #
    #       4A. `Meta_Metaclass::inspect`           -- This will be set to `property_inspect`
    #       4B. `Meta_Metaclass::inspect__result`       -- The inspection of meta metaclass `Meta_Metaclass`
    #
    #   Given the above, we can now say the following:
    #
    #       1.  `green.inspect`           -- Returns `Color::inspect`
    #       2.  `Color.inspect`           -- Returns `Color::inspect__result`          (via `Metacolor:inspect.__get__`)
    #       3.  `MetaColor.inspect`       -- Returns `MetaColor::inspect__result`      (via `Meta_Metaclass:inspect.__get__`)
    #       4.  `Meta_Metaclass.inspect`  -- Returns `Meta_Metaclass::inspect__result` (via `Meta_Metaclass:inspect.__get__`)
    #
    #   THUS:
    #
    #           `green.inspect` returns `Color::inspect`; BUT
    #           `Color.inspect` returns `Color::inspect__result`
    #
    #       Making a clear difference when inspecting *an instance* or *the class*.
    #
    class PropertyInspect(Object):
        __slots__ = (())


        def __get__(t, object, object_type):
            if object is none:
                #
                #   An alternate way to do the `return` statement is:
                #
                #       `return find_python_proxy_map_item(python_class_members(object_type), 'inspect__result')`
                #
                #   Which is slightly more accurate in that it means that:
                #
                #       1.  Use `::__inspect` instead of `._inspect`
                #       2.  Do not look in an sub-classes, but only the top level class
                #
               #assert object_type.inspect__result is find_python_proxy_map_item(python_class_members(object_type), 'inspect__result')

                return object_type.inspect__result

           #assert object.inspect__result is find_python_proxy_map_item(python_class_members(object), 'inspect__result')

            return object.inspect__result


        @static_method
        def __repr__():
            return '<PropertyInspect>'



    property_inspect = new_instance(PropertyInspect)


    share(
            'property_inspect',     property_inspect,
        )
