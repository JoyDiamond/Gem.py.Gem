#
#   Copyright (c) 2017-2018 Joy Diamond.  All rights reserved.
#
@module('Capital.Introspection')
def module():
    require_module('Capital.Absent')
    require_module('Capital.ProcessObjectMembers')


    #
    #   NOTE:
    #       This examins the method resolution order.
    #
    #       It will also frequently find keys multiple times.
    #
    #       In this situation, when finding a key, and deciding whether to ignore it or not, is is
    #       important to use `find_symbol` (again search the *WHOLE* method resolution order).
    #
    #       Here is an example:
    #
    #           M = Type('M', ((Type,)), process_type_members('M', Type, {}))
    #
    #       The method resolution order is:
    #
    #           M, Type, Object
    #
    #       We will find `__reduce__` two times (in `M` and `object).
    #
    #       When we find the key `__reduce__` in `Object.__dict__`, the question is *NOT* what
    #       is `Object.__reduce__`, but what does `find_symbol(M, "__reduce__")` return?
    #
    #       `find_symbol` will return `raise__CANNOT__reduce__ERROR` (which needs to be ignored)
    #       since it finds the symbol `__reduce__` in `M`.
    #
    #       *IF* we incorrectly looks in `Object`, we would get `Object.__reduce__` which should
    #       not be ignored; and thus, incorrect, not ignore it.
    #
    #       In other words -- DO *NOT* remove the call to `find_symbol` in the code ...
    #
    @export
    def introspect(m):
        key_set     = LiquidSet()
        add_key     = key_set.add
        find_symbol = 0

        for search in query__Type__method_resolution_order(m):
            search__class_members__keys = conjure__class_members__keys(search)

            for k in search__class_members__keys():
                ignore = lookup__hidden_introspection_key(k)

                if ignore is none:
                    add_key(k)
                    continue

                if find_symbol is 0:
                    find_symbol = produce__find_symbol(m)

                if ignore is find_symbol(k):
                    #line('introspect: ignore %s', k)
                    continue

                add_key(k)

        return sorted_tuple(key_set)


    @export
    def introspect_hidden(m):
        key_set = LiquidSet()

        for search in query__Type__method_resolution_order(m):
            search__class_members__keys = conjure__class_members__keys(search)

            key_set.update(search__class_members__keys())

        return sorted_tuple(key_set)
