#
#   Copyright (c) 2017-2018 Joy Diamond.  All rights reserved.
#
@module('Capital.Import')
def module():
    #
    #   import_module
    #
    if is_python_2:
        intern_string  = Shared.intern_string
        python_modules = Python_System.modules


        @export
        @privileged
        def import_module(module_name):
            module_name = intern_string(module_name)

            __import__(module_name)

            return python_modules[module_name]


    else:
        Python_ImportLibrary = __import__('importlib')


        #
        #   NOTE:
        #       Do not use 'export(import_module)' as this will *CHANGE* it's global scope to be
        #       Capital's shared scope -- which fails miserably since it can't find '_bootstrap' in the Capital scope.
        #
        #       Instead use two arguments, so its exported without having its global scope changed.
        #
        export(
            'import_module',    Python_ImportLibrary.import_module,
        )
