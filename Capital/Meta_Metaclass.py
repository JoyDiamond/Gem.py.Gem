#
#   Copyright (c) 2017-2018 Joy Diamond.  All rights reserved.
#
@module('Capital.Meta_Metaclass')
def module():
    require_module('Capital.Inspection_Of_A_Metaclass')
    require_module('Capital.ProcessTypeMembers')
    require_module('Capital.PropertyInspect')


    @share
    def portray_class(t):
        M = t.class_type

        if t is M:
            assert t is Meta_Metaclass

            #
            #   `Meta_Metaclass` is it's own Metaclass
            #
            #   NOTE:
            #       `Meta_Metaclass.__repr__` is called for all metaclasses that use `Meta_Metaclass` as the metaclass.
            #
            #       Thus `Meta_Metaclass.__repr__` is called, both for itself, and other metaclasses.  Thus, the `if`
            #       above is needed to distinguish these two cases.
            #
            return '<Meta_Metaclass ...>'

        return arrange('<%s %s>', M.class_name, t.class_name)


    #
    #   NOTE:
    #       See NOTE below as to why `Temporary_Metaclass` is [temporarily] needed.
    #
    class Temporary_Metaclass(Type):
        __slots__ = (())                #   `__slots__` not used, but declared for consistency


    Meta_Metaclass = create_python_type(
            Temporary_Metaclass,
            'Meta_Metaclass',
            base_classes__Type,
            process_type_members(
                'Meta_Metaclass',
                Type,
                {
#                   'inspect' : property_inspect,
                    'portray' : portray_class,
                },
            ),
        )


    Meta_Metaclass.inspect = create__Inspection_Of_A_Metaclass(Meta_Metaclass)


    #
    #   Make `Meta_Metaclass` it's own metaclass.
    #
    #   NOTE:
    #       Due to limitations of python, this require that `Meta_Metaclass` initially be created using
    #       `Temporary_Metaclass` as it's metaclass (instead of `Type`).
    #
    #       The following errors are returned if `Meta_Metaclass` is initially created with `Type` as
    #       it's metaclass:
    #
    #           Python 2: TypeError: __class__ assignment: only for heap types
    #           Python 3: TypeError: __class__ assignment only supported for heap types or ModuleType subclasses
    #
    #   NOTE:
    #       The next assignment Must use `.__class__` (instead of `.class_type` here, since it is *NOT* yet it's own
    #       metaclass, and thus does not have a `.class_type` member yet).
    #
    assert Meta_Metaclass.__class__ is Temporary_Metaclass

    Meta_Metaclass.__class__ = Meta_Metaclass


    #
    #   Now, after the previous assignment, as it's own metaclass, `Meta_Metaclass` does have a
    #   `.class_type` member
    #
    assert Meta_Metaclass.__class__ is Meta_Metaclass.class_type is Meta_Metaclass


    #
    #   Remove `.__slots__` (which is unused, but was added for consistency).
    #
    del Meta_Metaclass.__slots__


    export(
            'Meta_Metaclass',   Meta_Metaclass,
        )
