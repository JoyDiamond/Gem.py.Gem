#
#   Copyright (c) 2017-2018 Joy Diamond.  All rights reserved.
#
@module('Capital.Global')
def module():
    class Capital_Global(Object):
        __slots__ = (())


        if is_python_2:
            __slots__ += ((
                'context',              #   None | Exception
            ))


        __slots__ += ((
            'CRYSTAL_parser',           #   Boolean
            'JAVA_parser',              #   Boolean
            'PYTHON_parser',            #   Boolean
            'SQL_parser',               #   Boolean
            'TESTING',                  #   Boolean
            'TREMOLITE_parser',         #   Boolean
        ))


        def __init__(t):
            if is_python_2:
                t.context = none

            t.CRYSTAL_parser   = false
            t.JAVA_parser      = false
            t.PYTHON_parser    = false
            t.SQL_parser       = false
            t.TESTING          = false
            t.TREMOLITE_parser = false


    capital_global = Capital_Global()


    del Capital_Global.__init__


    export(
        'capital_global',   capital_global,
    )
