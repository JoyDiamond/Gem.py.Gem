#
#   Copyright (c) 2017-2018 Joy Diamond.  All rights reserved.
#
@module('Capital.Inspection_Of_A_Class')
def module():
    class Inspection_Of_A_Class(Object):
        __slots__ = ((
            'client',                   #   Type+
            'name',                     #   String+
        ))


        def __repr__(t):
            return arrange('<Inspection_Of_A_Class %r>', t.name)


    new__Inspection_Of_A_Class                = Method(new_instance, Inspection_Of_A_Class)
    initialize__Inspection_Of_A_Class__client = Inspection_Of_A_Class.client.__set__
    initialize__Inspection_Of_A_Class__name   = Inspection_Of_A_Class.name.__set__


    @share
    def create__Inspection_Of_A_Class(client):
        name = python_class_name(client)

        r = new__Inspection_Of_A_Class()

        initialize__Inspection_Of_A_Class__client(r, client)
        initialize__Inspection_Of_A_Class__name  (r, name)

        return r
