#
#   Copyright (c) 2018 Joy Diamond.  All rights reserved.
#
@module('Capital.TupleFunctions')
def module():
    require_module('Capital.Core')      #   Method, Tuple
    require_module('Capital.System')    #   slice_all


    @export
    def produce_append_freeze():
        many = []

        append = many.append
        freeze = Method(Tuple, many)

        return ((append, freeze))


    @export
    def produce_append_freeze_zap():
        many = []

        append = many.append
        freeze = Method(Tuple, many)
        zap    = Method(many.__delitem__, slice_all)

        return ((append, freeze, zap))
