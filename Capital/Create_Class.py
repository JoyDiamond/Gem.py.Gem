#
#   Copyright (c) 2017-2018 Joy Diamond.  All rights reserved.
#
@module('Capital.Create_Class')
def module():
    require_module('Capital.Capital_Object')
    require_module('Capital.Class_FindSymbol')
    require_module('Capital.Inspection_Of_A_Class')
    require_module('Capital.Meta_Metaclass')
    require_module('Capital.ProcessObjectMembers')
    require_module('Capital.StaticMethod_OnlyViaClass')


    #
    #   produce__create_class__only_via_class
    #
    #       (also helper routine `RAISE__create_class__only_via_the_metaclass__not_via_the_class__ERROR`)
    #
    def RAISE__create_class__only_via_the_metaclass__not_via_the_class__ERROR(object, object_type):
        assert object     .class_type is object_type
        assert object_type.class_type is Meta_Metaclass

        raise_attribute_error('`.create_class` may only be called via the metaclass %r; not via the class %r',
                              object_type, object)


    @share
    def produce__create_class__only_via_the_metaclass(M):
        M_Parent = M.class_parent

        find_symbol = produce__find_symbol(M_Parent)

        M_Parent__operator__new = find_symbol('__new__')

        if type(M_Parent__operator__new) is static_method:
            M_Parent__operator__new = M_Parent__operator__new.__get__(M_Parent)

        M_Parent__constructor = find_symbol('__init__')


        @rename('create_class__with_metaclass__%s', M.__name__)
        def create_class(name, members = 0, Parent = Capital_Object, newable = false):
            if members is 0:
                members = {}

            provide_member = members.setdefault

            provide_member('portray', portray_class)

            base_classes = (base_classes__Capital_Object   if Parent is Capital_Object else   ((Parent,)) )
            members      = process_object_members(name, members, newable = newable)

            R = M_Parent__operator__new(M, name, base_classes, members)

            if type(R) is M:
                if M_Parent__constructor is not Type__operator__constructor:
                    M_Parent__constructor(R, name, base_classes, members)

                R.inspect__result = create__Inspection_Of_A_Class(R)

            return R


        #
        #   Wrap `create_class` inside `StaticMethod_OnlyViaClass`, so it can only be used directly.
        #
        #       If trying to use via "an instance" produce an error with
        #       `RAISE__create_class__only_via_the_metaclass__not_via_the_class__ERROR`
        #
        #   NOTE:
        #       In the previous sentence "an instance" in this case means "an instance of the metaclass", or in other
        #       words a class ...
        #
        #       For more details, see "Capital/StaticMethod_OnlyViaClass.py" for an explanation of confusing terminology
        #       for "instance" and "class" here ... (which gets very confused with "class" and "metaclass" due to the
        #       actual types being used here being "classes" & "metaclasses").
        #
        return create__StaticMethod_OnlyViaClass(
                create_class,
                RAISE__create_class__only_via_the_metaclass__not_via_the_class__ERROR,
            )
