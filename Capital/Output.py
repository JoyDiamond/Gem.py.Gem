#
#   Copyright (c) 2017-2018 Joy Diamond.  All rights reserved.
#
@module('Capital.Output')
def module():
    standard_error  = Python_System.stderr
    standard_output = Python_System.stdout


    flush_standard_error  = standard_error .flush
    flush_standard_output = standard_output.flush
    write_standard_error  = standard_error .write
    write_standard_output = standard_output.write


    export(
        #
        #   Functions
        #
        'flush_standard_error',     flush_standard_error,
        'flush_standard_output',    flush_standard_output,
        'write_standard_error',     write_standard_error,
        'write_standard_output',    write_standard_output,
    )
