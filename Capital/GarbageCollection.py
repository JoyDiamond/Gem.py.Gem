#
#   Copyright (c) 2017-2018 Joy Diamond.  All rights reserved.
#
@module('Capital.GarbageCollection')
def module():
    require_module('Capital.Import')


    #
    #   Functions
    #
    Python_GarbaseCollection = import_module('gc')


    export(
        'collect_garbage',      Python_GarbaseCollection.collect,
    )
