#
#   Copyright (c) 2017-2018 Joy Diamond.  All rights reserved.
#
@module('Capital.Create_Metaclass')
def module():
    require_module('Capital.Create_Class')
    require_module('Capital.Meta_Metaclass')
    require_module('Capital.ProcessTypeMembers')
    require_module('Capital.TypeMembers')


    #
    #   create_metaclass
    #
    @export
    def create_metaclass(
            name,
            
            members       = 0,
            Parent        = Type, 
            abstractable  = false,
            callable      = false,
            constructable = false,
            newable       = false,
            subclassable  = false,
    ):
        if members is 0:
            members = {}

        provide_member = members.setdefault

        provide_member('portray', portray_class)
        provide_member('inspect', property_inspect)

        #
        #   NOTE:
        #       We always call `create_python_type` (i.e.: `Type.__new__`) because:
        #
        #           1.  We are creating an instance of `Meta_Metaclass`;
        #
        #           2.  The proper function to create an instance of `Meta_Metaclass` is `create_python_type`
        #
        #               (normally this would be inherited as `Meta_Metaclass.operator new`, except we
        #               deliberatly disabled `Meta_Metaclass.operator new`, with the idea we would call
        #               `create_python_type` where needed -- i.e.: here).
        #
        #       In other words, the following are irrelevant:
        #
        #           3.  We are *NOT* creating an instance of `Parent`; therefore we don't care how to create an
        #               instance of `Parent` (i.e.: by calling `Parent.__new__`).
        #
        #           4.  We are *NOT* creating an instance of the metaclass of `Parent`; therefore we don't care how to
        #               create an instance of the metaclass of `Parent` (i.e.: by calling `type(parent).__new__`).
        #
        Metaclass = create_python_type(
                Meta_Metaclass,
                name,
                (base_classes__Type   if Parent is Type else   ((Parent,)) ),
                process_type_members(
                    name, Parent, members,

                    abstractable  = abstractable,
                    callable      = callable,
                    constructable = constructable,
                    newable       = newable,
                    subclassable  = subclassable,
                ),
            )

        del Metaclass.__slots__

        Metaclass.inspect__result = create__Inspection_Of_A_Metaclass            (Metaclass)
        Metaclass.create_class    = produce__create_class__only_via_the_metaclass(Metaclass)

        return Metaclass
