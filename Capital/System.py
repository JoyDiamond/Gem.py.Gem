#
#   Copyright (c) 2017-2018 Joy Diamond.  All rights reserved.
#
@module('Capital.System')
def module():
    #
    #   Functions
    #
    python_frame = Python_System._getframe


    #
    #   Function with bound parameter
    #
    caller_frame_1 = Method(python_frame, 1)


    @export
    def my_name():
        return caller_frame_1().f_code.co_name


    @export
    def my_line(format = none, *arguments):
        if format is none:
            assert length(arguments) is 0

            write_standard_output(caller_frame_1().f_code.co_name + '\n')
        else:
            write_standard_output(
                    (
                          caller_frame_1().f_code.co_name
                        + ': '
                        + (format % arguments   if arguments else   format)
                        + '\n'
                    ),
                )

        flush_standard_output()


    export(
        'caller_frame_1',           caller_frame_1,
        'change_check_interval',    Python_System.setcheckinterval,
        'fetch_check_interval',     Python_System.getcheckinterval,
        'module_path',              Python_System.path,
        'program_exit',             Python_System.exit,
        'python_frame',             Python_System._getframe,
        'python_version',           Python_System.version,
        'reference_count',          Python_System.getrefcount,
        'slice_all',                Slice(none, none),
    )


    if is_python_2:
        export(
            'MAXIMUM_INTEGER',      Python_System.maxint,
        )
