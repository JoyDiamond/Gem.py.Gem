#
#   Copyright (c) 2017-2018 Joy Diamond.  All rights reserved.
#
@module('Capital.Inspection_Of_A_Metaclass')
def module():
    class Inspection_Of_A_Metaclass(Object):
        __slots__ = ((
            'client',                   #   Type+
            'name',                     #   String+
        ))


        def __repr__(t):
            return arrange('<Inspection_Of_A_Metaclass %r>', t.name)



    new__Inspection_Of_A_Metaclass                = Method(new_instance, Inspection_Of_A_Metaclass)
    initialize__Inspection_Of_A_Metaclass__client = Inspection_Of_A_Metaclass.client.__set__
    initialize__Inspection_Of_A_Metaclass__name   = Inspection_Of_A_Metaclass.name.__set__


    @share
    def create__Inspection_Of_A_Metaclass(client):
        name = python_class_name(client)

        r = new__Inspection_Of_A_Metaclass()

        initialize__Inspection_Of_A_Metaclass__client(r, client)
        initialize__Inspection_Of_A_Metaclass__name  (r, name)

        return r
