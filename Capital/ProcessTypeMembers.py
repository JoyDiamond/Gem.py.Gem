#
#  Copyright (c) 2018 Joy Diamond.  All rights reserved.
#
@module('Capital.ProcessTypeMembers')
def module():
    show = 0


    require_module('Capital.ProcessObjectMembers')
    require_module('Capital.TypeMembers')


    #
    #   PREPARE__classes_with_metaclass__X__cannot__Y__ERROR
    #
    def PREPARE__classes_with_metaclass__X__cannot__Y__ERROR(m, function_name, description):
        m_name         = m.__name__
        metaclass_name = m.__class__.__name__

        return PREPARE_attribute_ERROR("`%s.%s`: class `%s` with metaclass `%s` cannot %s",
                                       m_name, function_name, m_name, metaclass_name, description)


    #
    #   PREPARE__invalid_member__X__use__Y__instead__ERROR
    #   PREPARE__invalid_method__X__use__Y__instead__ERROR
    #
    def PREPARE__invalid_member__X__use__Y__instead__ERROR(m, member_name, instead_name):
        m_name = m.__name__

        return PREPARE_attribute_ERROR("`%s.%s`: invalid member `.%s`; use `.%s` instead",
                                       m_name, member_name, member_name, instead_name)


    def PREPARE__invalid_method__X__use__Y__instead__ERROR(m, method_name, instead_name):
        m_name = m.__name__

        return PREPARE_attribute_ERROR("`%s.%s`: invalid method `.%s`; use `.%s` instead",
                                       m_name, method_name, method_name, instead_name)


    #
    #   raise__CANNOT__abstract_methods__ERROR
    #
    @property_3
    def raise__CANNOT__abstract_methods__ERROR(t, *arguments):
        attribute_error = PREPARE__classes_with_metaclass__X__cannot__Y__ERROR(
                t,
                'operator abstract methods (__abstractmethods__)',
                'use abstract methods'
            )

        raise attribute_error


    #
    #   raise__CANNOT__call__ERROR
    #
    @property_3
    def raise__CANNOT__call__ERROR(t, *arguments):
        attribute_error = PREPARE__classes_with_metaclass__X__cannot__Y__ERROR(
                t,
                'operator call (__call__)',
                'be called with function call syntax'
            )

        raise attribute_error


    #
    #   raise__CANNOT__instance_check__ERROR
    #   raise__CANNOT__subclass_check__ERROR
    #
    @property_3
    def raise__CANNOT__instance_check__ERROR(m):
        attribute_error = PREPARE__class__X__cannot__Y__ERROR(
                m,
                "operator instance check (__instancecheck__)",
                "check if an instance is of it's class type",
            )

        raise attribute_error


    @property_3
    def raise__CANNOT__subclass_check__ERROR(m):
        attribute_error = PREPARE__class__X__cannot__Y__ERROR(
                m,
                "operator subclass check (__subclasscheck__)",
                "check if another class is it's subclass",
            )

        raise attribute_error


    #
    #   raise__INVALID_MEMBER__base__ERROR
    #
    @property_3
    def raise__INVALID_MEMBER__first_base__ERROR(t):
        attribute_error = PREPARE__invalid_member__X__use__Y__instead__ERROR(t, '__base__', 'class_parent')

        raise attribute_error


    @property_3
    def raise__INVALID_MEMBER__basicsize__ERROR(t):
        attribute_error = PREPARE__invalid_member__X__use__Y__instead__ERROR(t, '__basicsize__', 'class_basic_size')

        raise attribute_error


    @property_3
    def raise__INVALID_MEMBER__dictoffset__ERROR(t):
        attribute_error = PREPARE__invalid_member__X__use__Y__instead__ERROR(t, '__dictoffset__', 'class_members_offset')

        raise attribute_error


    @property_3
    def raise__INVALID_MEMBER__flags__ERROR(t):
        attribute_error = PREPARE__invalid_member__X__use__Y__instead__ERROR(t, '__flags__', 'class_flags')

        raise attribute_error


    @property_3
    def raise__INVALID_MEMBER__itemsize__ERROR(t):
        attribute_error = PREPARE__invalid_member__X__use__Y__instead__ERROR(t, '__itemsize__', 'class_item_size')

        raise attribute_error


    @property_3
    def raise__INVALID_MEMBER__weakrefoffset__ERROR(t):
        attribute_error = PREPARE__invalid_member__X__use__Y__instead__ERROR(
                t,
                '__weakrefoffset__',
                'class_weak_reference_offset',
            )

        raise attribute_error


    #
    #   raise__INVALID_METHOD__subclasses__ERROR
    #
    @property_3
    def raise__INVALID_METHOD__subclasses__ERROR(t):
        attribute_error = PREPARE__invalid_method__X__use__Y__instead__ERROR(
                t,
                "operator find subclasses (__subclasses__)",
                "class_immediate_subclasses",
            )

        raise attribute_error


    #
    #   process_type_members
    #
    @export
    def process_type_members(
            name, parent, members,

            provide_member = 0,
            abstractable   = false,
            callable       = false,
            constructable  = false,
            introspectable = true,
            newable        = false,
            subclassable   = false,
    ):
        if show:
            line('process_type_members(%r, %r, %r ...)', name, parent, members)

        find_symbol = produce__find_symbol(parent, show = show)

        member__class_bases                             = find_symbol('__bases__')
        member__class_calculate_method_resolution_order = find_symbol('mro')
        member__class_type                              = find_symbol('__class__')
        member__method_resolution_order                 = find_symbol('__mro__')
        member__class_name                              = find_symbol('__name__')
        member__class_members                           = find_symbol('__dict__')

        pop = members.pop

        portray = pop('portray', find_symbol('__repr__'))

        if FrozenSet(members) & reserved_member_names:
            raise__reserved_names__error('process_type_members', name, members, reserved_member_names)

        if provide_member is 0:
            provide_member = members.setdefault


        #
        #   Always provide a `slots` members, if missing
        #
        #
        #   NOTE:
        #       This is useless for classes derived from `Type` -- but it is done anyway, for consistency.
        #
        provide_member('__slots__', (()) )

        #
        #   All members from `Object` (except for `doc`)
        #
        provide_member('__class__',        member__class_type)
        provide_member('__delattr__',      find_symbol('__delattr__'))
       #provide_member('__doc__',          find_symbol('__doc__'))
        provide_member('__format__',       raise__CANNOT__format__ERROR)
        provide_member('__getattribute__', find_symbol('__getattribute__'))         #   tp_getattr
        provide_member('__hash__',         find_symbol('__hash__'))                 #   tp_hash

        provide_member(
                '__init__',                                                         #   tp_init
                (
                    find_symbol('__init__')   if constructable else
                    raise__CANNOT__construct__ERROR
                ),
            )

        provide_member(
                '__new__',                                                          #   tp_new
                (
                    find_symbol('__new__')   if newable else
                    raise__CANNOT__create__ERROR
                ),
            )

        provide_member('__reduce__',       raise__CANNOT__reduce__ERROR)
        provide_member('__reduce_ex__',    raise__CANNOT__reduce_extended__ERROR)
        provide_member('__repr__',         portray)                                 #   tp_repr
        provide_member('__setattr__',      find_symbol('__setattr__'))              #   tp_setattr
        provide_member('__sizeof__',       find_symbol('__sizeof__'))
        provide_member('__str__',          find_symbol('__str__'))                  #   tp_str
        provide_member('__subclasshook__', raise__CANNOT__subclass_hook__ERROR)

        if introspectable:
            if is_python_3:
                provide_member('__dir__',  find_symbol('__dir__'))
        else:
            #
            #   NOTE:
            #       Although `Object.__dir__` is *ONLY* defined in python 3:
            #
            #           `__dir__` is called by *BOTH* python 2 & 3, if it exists.
            #
            #       Therefore, we declare in *BOTH* python 2 & 3.
            #
            provide_member('__dir__',      raise__CANNOT__introspection__ERROR)

        #
        #   Comparison operators
        #
        if is_python_2:
            provide_member('__cmp__',      operator__compare__NOT_ALLOWED)

        provide_member('__eq__',           operator__equal__NOT_ALLOWED)
        provide_member('__gt__',           operator__greater_than__NOT_ALLOWED)
        provide_member('__ge__',           operator__greater_than_or_equal__NOT_ALLOWED)
        provide_member('__lt__',           operator__less_than__NOT_ALLOWED)
        provide_member('__le__',           operator__less_than_or_equal__NOT_ALLOWED)
        provide_member('__ne__',           operator__not_equal__NOT_ALLOWED)


        #
        #   All members from `Type` (that are not also members of `Object`)
        #
        provide_member(
                '__abstractmethods__',
                (
                    find_symbol('__abstractmethods__')   if abstractable else
                    raise__CANNOT__abstract_methods__ERROR
                ),
            )

        provide_member('__base__',                raise__INVALID_MEMBER__first_base__ERROR)
        provide_member('__bases__',               member__class_bases)
        provide_member('__basicsize__',           raise__INVALID_MEMBER__basicsize__ERROR)

        provide_member(
                '__call__',
                (
                    find_symbol('__call__')   if callable else
                    raise__CANNOT__call__ERROR
                ),
            )
        
        provide_member('__dict__',                member__class_members)
        provide_member('__dictoffset__',          raise__INVALID_MEMBER__dictoffset__ERROR)
        provide_member('__flags__',               raise__INVALID_MEMBER__flags__ERROR)
        provide_member('__instancecheck__',       raise__CANNOT__instance_check__ERROR)
        provide_member('__itemsize__',            raise__INVALID_MEMBER__itemsize__ERROR)
        provide_member('mro',                     member__class_calculate_method_resolution_order)
        provide_member('__mro__',                 find_symbol('__mro__'))
        provide_member('__name__',                member__class_name)

        provide_member(
                '__subclasscheck__',
                (
                    find_symbol('__subclasscheck__')   if subclassable else
                    raise__CANNOT__subclass_check__ERROR
                ),
            )

        provide_member('__subclasses__',          raise__INVALID_METHOD__subclasses__ERROR)
        provide_member('__weakrefoffset__',       raise__INVALID_MEMBER__weakrefoffset__ERROR)


        #
        #   New names
        #
        #   NOTE:
        #       The following appear under both the old & new names:
        #
        #           `.__class__`        `.class_type`
        #           `.__dict__`         `.class_members`
        #           `.__mro__`          `.class_method_resolution_order`
        #           `.__name__`         `.class_name`
        #
        provide_member('class_bases',                               member__class_bases)
        provide_member('class_basic_size',                          find_symbol('__basicsize__'))
        provide_member('class_calculate_method_resolution_order',   member__class_calculate_method_resolution_order)
        provide_member('class_flags',                               find_symbol('__flags__'))
        provide_member('class_immediate_subclasses',                find_symbol('__subclasses__'))
        provide_member('class_item_size',                           find_symbol('__itemsize__'))
        provide_member('class_members',                             member__class_members)
        provide_member('class_members_offset',                      find_symbol('__dictoffset__'))
        provide_member('class_method_resolution_order',             member__method_resolution_order)
        provide_member('class_name',                                member__class_name)
        provide_member('class_parent',                              find_symbol('__base__'))
        provide_member('class_type',                                member__class_type)
        provide_member('class_weak_reference_offset',               find_symbol('__weakrefoffset__'))

        return members


    hidden_introspection_keys['__abstractmethods__'] = raise__CANNOT__abstract_methods__ERROR
    hidden_introspection_keys['__base__']            = raise__INVALID_MEMBER__first_base__ERROR
    hidden_introspection_keys['__bases__']           = Type__member__class_bases    #   Use `.class_bases` instead
    hidden_introspection_keys['__basicsize__']       = raise__INVALID_MEMBER__basicsize__ERROR
    hidden_introspection_keys['__call__']            = raise__CANNOT__call__ERROR
    hidden_introspection_keys['__dictoffset__']      = raise__INVALID_MEMBER__dictoffset__ERROR
    hidden_introspection_keys['__dict__']            = python_class_members         #   Use `.class_members` instead
    hidden_introspection_keys['__flags__']           = raise__INVALID_MEMBER__flags__ERROR
    hidden_introspection_keys['__name__']            = Type__member__class_name     #   Use `.class_name` instead


    #
    #   Use `.class_method_resolution_order` instead of `.__mro__`
    #
    hidden_introspection_keys['__mro__']           = Type__member__method_resolution_order
    hidden_introspection_keys['__instancecheck__'] = raise__CANNOT__instance_check__ERROR
    hidden_introspection_keys['__itemsize__']      = raise__INVALID_MEMBER__itemsize__ERROR
    hidden_introspection_keys['__subclasscheck__'] = raise__CANNOT__subclass_check__ERROR
    hidden_introspection_keys['__subclasses__']    = raise__INVALID_METHOD__subclasses__ERROR
    hidden_introspection_keys['__weakrefoffset__'] = raise__INVALID_MEMBER__weakrefoffset__ERROR


    #
    #   Use `.class_calculate_method_resolution_order` instead of `.mro`
    #
    hidden_introspection_keys['mro'] = Type__member__class_calculate_method_resolution_order
