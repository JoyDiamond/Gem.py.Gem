#
#   Copyright (c) 2017-2018 Joy Diamond.  All rights reserved.
#
@module('Capital.IO')
def module():
    export(
        #
        #   Insanely enough, the python 2 'input' function actually evaluated the input!
        #   We use the python 3 meaning of 'input' -- don't evaluate the input
        #
        'input',        (Python_BuiltIn.raw_input   if is_python_2 else   Python_BuiltIn.input),
    )
