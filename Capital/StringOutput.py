#
#   Copyright (c) 2017-2018 Joy Diamond.  All rights reserved.
#
@module('Capital.StringOutput')
def module():
    require_module('Capital.ChangePrefix')
    require_module('Capital.SimpleStringIO')


    #
    #   StringOutput
    #
    #       `prefix`:          Prefix for a normal line of text
    #       `prefix_blanks`:   `prefix` (striped of trailing spaces) + '\n'
    #
    @export
    class StringOutput(Object):
        __slots__ = ((
            'f',                        #   StringIO
            'prefix',                   #   String_0
            'prefix_blanks',            #   String
            'result',                   #   String_0
            'total_blanks',             #   Integer
            'position',                 #   Integer
            'write',                    #   Method

            'method__query_position',           #   Method_0
            'method__alter_prefix',             #   Method_0
            'method__alter_prefix_blanks',      #   Method_0
            'method__line',                     #   Method_0
        ))


        def __init__(t, f):
            t.f             = f
            t.prefix        = 0
            t.prefix_blanks = '\n'
            t.result        = 0
            t.total_blanks  = -1
            t.position      = 0
            t.write         = f.write

            t.method__query_position      = 0
            t.method__alter_prefix        = 0
            t.method__alter_prefix_blanks = 0
            t.method__line                = 0


        def __enter__(t):
            return t


        def __exit__(t, e_type, e, e_traceback):
            if e_type is none:
                t.finish()
            else:
                t.close()


        def blank(t):
            assert t.position is 0

            if t.total_blanks is 0:
                t.total_blanks = 1


        def blank2(t):
            assert t.position is 0

            if 0 <= t.total_blanks < 2:
                t.total_blanks = 2


        def blank_suppress(t):
            assert t.position is 0

            t.total_blanks = -1


        def close(t):
            f       = t.f
            t.write = t.f = none

            if f is not none:
                f.close()


        def flush(t):
            if t.position is 1:
                t.write('\n')
                t.position = 0

            if t.total_blanks > 0:
                t.write(t.prefix_blanks * t.total_blanks)

            t.total_blanks = 0


        def indent(t, header = 0, ending = 0, prefix = 4):
            if header is not 0:
                t.line(header)

            if ending is 0:
                method__query_position = 0
                method__line           = 0
            else:
                #
                #   method__query_position
                #
                method__query_position = t.method__query_position

                if method__query_position is 0:
                    method__query_position = t.method__query_position = Method(query__StringOutput__position, t)

                #
                #   method__line
                #
                method__line = t.method__line

                if method__line is 0:
                    method__line = t.method__line = t.line

            #
            #   method__alter_prefix
            #
            method__alter_prefix = t.method__alter_prefix

            if method__alter_prefix is 0:
                method__alter_prefix = t.method__alter_prefix = Method(alter__StringOutput__prefix, t)


            #
            #   old__prefix & new__prefix
            #
            old__prefix = t.prefix

            if old__prefix is 0:
                new__prefix = prefix * ' '
            else:
                new__prefix = old__prefix + prefix * ' '

            return ChangePrefix(
                        old__prefix,                    #   old__prefix
                        0,                              #   old__prefix_blanks
                        method__query_position,         #   method__query_position
                        method__alter_prefix,           #   method__alter_prefix
                        0,                              #   method__alter_prefix_blanks
                        method__line,                   #   method__line
                        ending,                         #   ending
                        new__prefix,                    #   new__prefix
                        0,                              #   new__prefix_blanks
                   )


        def change_prefix(t, prefix):
            assert t.prefix is 0

            prefix_blanks = prefix.rstrip()

            if prefix_blanks is '':
                prefix_blanks               = 0
                method__alter_prefix_blanks = 0
            else:
                assert length(prefix_blanks) > 0

                prefix_blanks += '\n'

                #
                #   method__alter_prefix_blanks
                #
                method__alter_prefix_blanks = t.method__alter_prefix_blanks

                if method__alter_prefix_blanks is 0:
                    method__alter_prefix_blanks = t.method__alter_prefix_blanks = (
                            Method(alter__StringOutput__prefix_blanks, t)
                        )

            #
            #   method__alter_prefix
            #
            method__alter_prefix = t.method__alter_prefix

            if method__alter_prefix is 0:
                method__alter_prefix = t.method__alter_prefix = Method(alter__StringOutput__prefix, t)

            return ChangePrefix(
                        0,                                                      #   old__prefix
                        (0   if prefix_blanks is 0 else   t.prefix_blanks),     #   old__prefix_blanks
                        0,                                                      #   method__query_position
                        method__alter_prefix,                                   #   method__alter_prefix
                        method__alter_prefix_blanks,                            #   method__alter_prefix_blanks
                        0,                                                      #   method__line
                        0,                                                      #   ending
                        prefix,                                                 #   new__prefix
                        prefix_blanks,                                          #   new__prefix_blanks
                   )


        def finish(t):
            r = t.result = t.f.getvalue()

            t.close()

            return r


        def line(t, format = none, *arguments):
            if t.position:
                #
                #   Finish current line (does not alter `total_blanks`)
                #
                t.position = 0

                if format is none:
                    assert length(arguments) is 0

                    t.write('\n')
                    return

                s = (format % arguments   if arguments else   format)

                t.write(s + '\n')
                return

            total_blanks = t.total_blanks

            if format is none:
                assert length(arguments) is 0

                #
                #   Always write this [explicit] blank line out (even if blanks are currently disabled; or *IF* blanks
                #   are disabled in the future).
                #
                t.write(t.prefix_blanks)

                if total_blanks > 0:
                    t.blanks = total_blanks - 1

                return

            prefix = t.prefix
            s      = (format % arguments   if arguments else   format)

            if prefix is 0:
                if total_blanks > 0:
                    t.write(t.prefix_blanks * total_blanks + s + '\n')
                else:
                    t.write(s + '\n')
            else:
                if total_blanks > 0:
                    t.write(t.prefix_blanks * total_blanks + prefix + s + '\n')
                else:
                    t.write(prefix + s + '\n')

            #
            #   Always `.total_blanks = 0`; that way even if was `-1` it is reset to `0`.
            #
            t.total_blanks = 0


        def partial(t, format, *arguments):
            s = (format % arguments   if arguments else   format)

            position = t.position

            total = position + length(s)

            if position > 0:
                #
                #   Finish current line (does not alter `total_blanks`)
                #
                t.write(s)
            else:
                prefix       = t.prefix
                total_blanks = t.total_blanks

                if prefix is 0:
                    if total_blanks > 0:
                        t.write(t.prefix_blanks * total_blanks + s)
                    else:
                        t.write(s)
                else:
                    total += length(prefix)

                    if total_blanks > 0:
                        t.write(t.prefix_blanks * total_blanks + prefix + s)
                    else:
                        t.write(prefix + s)

                t.total_blanks = 0

            t.position = total


        def write_multiline(t, multiline):
            data = multiline.splitlines()
            line = t.line

            i          = 0
            maximum_m1 = length(data) - 1

            for s in data:
                if i == maximum_m1:
                    t.partial(s)
                    return

                line(s)
                i += 1


    alter__StringOutput__prefix_blanks = StringOutput.prefix_blanks.__set__
    alter__StringOutput__prefix        = StringOutput.prefix       .__set__
    query__StringOutput__position      = StringOutput.position     .__get__


    @export
    def create_StringOutput():
        return StringOutput(create_SimpleStringOutput())
