#
#  Copyright (c) 2018 Joy Diamond.  All rights reserved.
#
@module('Capital.Class_ShowMembers')
def module():
    @export
    def show_class_members(m, show_hidden = 0):
        introspect_function = (introspect_hidden   if show_hidden else   introspect)

        with indent(arrange('%s:', m.__name__), prefix = 2):
            for k in introspect_function(m):
                [symbol, base, depth] = find_symbol_base_and_depth(m, k)

                if depth == 1:
                    line('%s: %r', k, symbol)
                else:
                    line('%s @ `%s` (#%d): %r', k, base.__name__, depth, symbol)
