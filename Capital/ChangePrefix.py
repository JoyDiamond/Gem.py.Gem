#
#   Copyright (c) 2017-2018 Joy Diamond.  All rights reserved.
#
@module('Capital.ChangePrefix')
def module():
    @share
    class ChangePrefix(Object):
        __slots__ = ((
            'old__prefix',                  #   String_0
            'old__prefix_blanks',           #   String_0
            'method__query_position',       #   Method_0
            'method__alter_prefix',         #   Method
            'method__alter_prefix_blanks',  #   Method_0
            'method__line',                 #   Method_0
            'ending',                       #   String_0
            'new__prefix',                  #   String
            'new__prefix_blanks',           #   String_0
        ))


        def __init__(
                t, old__prefix, old__prefix_blanks,
                method__query_position, method__alter_prefix, method__alter_prefix_blanks, method__line,
                ending, new__prefix, new__prefix_blanks,
        ):
            if method__query_position is 0:
                assert ending is 0
            else:
                assert ending is not 0

            if method__alter_prefix_blanks is 0:
                assert old__prefix_blanks  is new__prefix_blanks is 0
            else:
                assert old__prefix_blanks is not 0
                assert new__prefix_blanks is not 0

            if method__line is 0:
                assert ending is 0
            else:
                assert ending is not 0

            t.old__prefix                 = old__prefix
            t.old__prefix_blanks          = old__prefix_blanks
            t.method__query_position      = method__query_position
            t.method__alter_prefix        = method__alter_prefix
            t.method__alter_prefix_blanks = method__alter_prefix_blanks
            t.method__line                = method__line
            t.ending                      = ending
            t.new__prefix                 = new__prefix
            t.new__prefix_blanks          = new__prefix_blanks


        def __enter__(t):
            t.method__alter_prefix(t.new__prefix)

            method__alter_prefix_blanks = t.method__alter_prefix_blanks

            if method__alter_prefix_blanks is 0:
                assert t.new__prefix_blanks is 0
            else:
                method__alter_prefix_blanks(t.new__prefix_blanks)

            return t


        def __exit__(t, e_type, e, e_traceback):
            t.method__alter_prefix(t.old__prefix)

            method__alter_prefix_blanks = t.method__alter_prefix_blanks

            if method__alter_prefix_blanks is 0:
                assert t.old__prefix_blanks is 0
            else:
                method__alter_prefix_blanks(t.old__prefix_blanks)

            if e is none:
                ending = t.ending

                if ending is not 0:
                    method__line = t.method__line

                    if t.method__query_position():
                        method__line()

                    method__line(ending)
