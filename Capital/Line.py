#
#   Copyright (c) 2017-2018 Joy Diamond.  All rights reserved.
#
@module('Capital.Line')
def module():
    require_module('Capital.ChangePrefix')


    alter__standard_output__prefix_blanks         = Shared.alter__standard_output__prefix_blanks
    alter__standard_output__prefix                = Shared.alter__standard_output__prefix
    alter__standard_output__total_blanks__1       = Shared.alter__standard_output__total_blanks__1
    alter__standard_output__total_blanks__MINUS_1 = Shared.alter__standard_output__total_blanks__MINUS_1
    alter__standard_output__total_blanks          = Shared.alter__standard_output__total_blanks
    query__standard_output__position              = Shared.query__standard_output__position
    query__standard_output__prefix_blanks         = Shared.query__standard_output__prefix_blanks
    query__standard_output__prefix                = Shared.query__standard_output__prefix
    query__standard_output__total_blanks          = Shared.query__standard_output__total_blanks


    del Shared.alter__standard_output__prefix
    del Shared.alter__standard_output__prefix_blanks
    del Shared.alter__standard_output__total_blanks
    del Shared.alter__standard_output__total_blanks__1
    del Shared.alter__standard_output__total_blanks__MINUS_1
    del Shared.query__standard_output__position
    del Shared.query__standard_output__prefix
    del Shared.query__standard_output__prefix_blanks
    del Shared.query__standard_output__total_blanks


    @export
    def blank():
        assert query__standard_output__position() is 0

        if query__standard_output__total_blanks() is 0:
            alter__standard_output__total_blanks__1()


    @export
    def blank_suppress():
        assert query__standard_output__position() is 0

        alter__standard_output__total_blanks__MINUS_1()


    @export
    def change_prefix(prefix):
        old__prefix        = query__standard_output__prefix()
        old__prefix_blanks = query__standard_output__prefix_blanks()

        assert old__prefix is ''

        prefix_blanks = prefix.rstrip()

        if prefix_blanks is '':
            prefix_blanks               = 0
            method__alter_prefix_blanks = 0
        else:
            assert length(prefix_blanks) > 0

            prefix_blanks += '\n'

            method__alter_prefix_blanks = alter__standard_output__prefix_blanks

        return ChangePrefix(
                    old__prefix                                                     #   old__prefix
                    (0   if old__prefix_blanks is 0 else   old__prefix_blanks),     #   old__prefix_blanks
                    0,                                                              #   method__query_position
                    alter__standard_output__prefix,                                 #   method__alter_prefix
                    method__alter_prefix_blanks,                                    #   method__alter_prefix_blanks
                    0,                                                              #   method__line
                    0,                                                              #   ending
                    prefix,                                                         #   new__prefix
                    prefix_blanks,                                                  #   new__prefix_blanks
               )



    @export
    def indent(header = 0, ending = 0, prefix = 4):
        if header is not 0:
            line(header)

        old__prefix = query__standard_output__prefix()

        if old__prefix is 0:
            new__prefix = prefix * ' '
        else:
            new__prefix = old__prefix + prefix * ' '

        return ChangePrefix(
                    old__prefix,                                                    #   old__prefix
                    0,                                                              #   old__prefix_blanks
                    (0   if ending is 0 else   query__standard_output__position),   #   method__query_position
                    alter__standard_output__prefix,                                 #   method__alter_prefix
                    0,                                                              #   method__alter_prefix_blanks
                    (0    if ending is 0 else   line),                              #   method__line
                    ending,                                                         #   ending
                    new__prefix,                                                    #   new__prefix
                    0,                                                              #   new__prefix_blanks
               )
