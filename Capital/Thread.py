#
#   Copyright (c) 2018 Joy Diamond.  All rights reserved.
#
@module('Capital.Thread')
def module():
    require_module('Capital.Import')


    Python_Thread = import_module('thread')


    export(
        'allocate_lock',        Python_Thread.allocate_lock,
        'start_new_thread',     Python_Thread.start_new_thread,
        'thread_identifier',    Python_Thread.get_ident,
    )
