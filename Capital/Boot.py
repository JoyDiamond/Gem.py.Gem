#
#   Copyright (c) 2017-2018 Joy Diamond.  All rights reserved.
#
def module(module_name):
    def execute(f):
        return f()

    return execute


@module('Capital.Boot')
def module():
    #
    #   This really belongs in Capital.Core, but is here since we need it during Boot
    #
    Python_System    = __import__('sys')
    Python_Types     = __import__('types')
    is_python_2      = (Python_System.version_info.major is 2)
    is_python_3      = (Python_System.version_info.major is 3)
    Python_BuiltIn   = __import__('__builtin__'  if is_python_2 else   'builtins')
    Python_Exception = (__import__('exceptions')   if is_python_2 else  Python_BuiltIn)


    #
    #   Python keywords
    #
    false = False
    none  = None
    true  = True


    #
    #   Python Functions
    #
    intern_string = (Python_BuiltIn   if is_python_2 else   Python_System).intern
    is_instance   = isinstance
    iterate       = Python_BuiltIn.iter
    length        = Python_BuiltIn.len


    #
    #   Python Types
    #
    Module    = Python_BuiltIn.__class__
    Object    = Python_BuiltIn.object
    String    = Python_BuiltIn.str
    Traceback = Python_Types.TracebackType


    #
    #   python_modules (also lookup_python_module & store_python_module)
    #
    python_modules       = Python_System.modules
    lookup_python_module = python_modules.get
    store_python_module  = python_modules.__setitem__


    if is_python_3:
        delete_python_module = python_modules.__delitem__


    #
    #   Python values
    #
    python_debug_mode = Python_BuiltIn.__debug__


    #
    #   Capital
    #
    Capital                 = python_modules['Capital']
    Capital_name            = Capital.__name__ = intern_string(Capital.__name__)
    module_annotation_scope = Capital.__dict__


    #
    #   CapitalBuiltIn
    #
    CapitalBuiltIn       = Module(intern_string('Capital.BuiltIn'))
    CapitalBuiltIn_scope = CapitalBuiltIn.__dict__


    #
    #   CapitalPrivileged
    #
    CapitalPrivileged       = Module(intern_string('Capital.Privileged'))
    CapitalPrivileged_scope = CapitalPrivileged.__dict__

    CapitalPrivileged.__builtins__ = Python_BuiltIn.__dict__


    #
    #   CapitalShared
    #
    CapitalShared       = Module(intern_string('Capital.Shared'))
    CapitalShared_scope = CapitalShared.__dict__


    #
    #   Store in python modules:
    #
    #       Capital                 (overwritten)
    #       Capital.BuiltIn
    #       Capital.Privileged
    #       Capital.Shared
    #
    for module in [Capital, CapitalBuiltIn, CapitalPrivileged, CapitalShared]:
        store_python_module(module.__name__, module)


    #
    #   Debugging
    #
    if 7:
        flush_standard_output = Python_System.stdout.flush
        write_standard_output = Python_System.stdout.write


        def debug(format = none, *arguments):
            if format is none:
                assert length(arguments) is 0

                write_standard_output('\n')
            else:
                write_standard_output((format % arguments   if arguments else   format) + '\n')

            flush_standard_output()


    #
    #   boot
    #
    def boot():
        del Capital.boot, Capital.Boot, python_modules['Capital.Boot']


    #
    #   Function
    #
    Function = boot.__class__

    if is_python_2:
        function_closure  = Function.func_closure .__get__
        function_code     = Function.func_code    .__get__
        function_defaults = Function.func_defaults.__get__
        function_globals  = Function.func_globals .__get__
    else:
        function_closure  = Function.__closure__ .__get__
        function_code     = Function.__code__    .__get__
        function_defaults = Function.__defaults__.__get__
        function_globals  = Function.__globals__ .__get__

    function_name = Function.__dict__['__name__'].__get__


    #
    #   Code
    #
    if python_debug_mode:
        Code = function_code(boot).__class__


        code_argument_count    = Code.co_argcount   .__get__
        code_cell_vars         = Code.co_cellvars   .__get__
        code_constants         = Code.co_consts     .__get__
        code_filename          = Code.co_filename   .__get__
        code_first_line_number = Code.co_firstlineno.__get__
        code_flags             = Code.co_flags      .__get__
        code_free_variables    = Code.co_freevars   .__get__
        code_global_names      = Code.co_names      .__get__
        code_line_number_table = Code.co_lnotab     .__get__
        code_name              = Code.co_name       .__get__
        code_number_locals     = Code.co_nlocals    .__get__
        code_stack_size        = Code.co_stacksize  .__get__
        code_variable_names    = Code.co_varnames   .__get__
        code_virtual_code      = Code.co_code       .__get__

        if not is_python_2:
            code_keyword_only_argument_count = Code.co_kwonlyargcount.__get__


    #
    #   localize & privilege
    #
    #       Although it is possible to share code between 'localize' & 'privileged' -- it doesn't make
    #       sense -- since 'localize' is local to this function.  Thus when this when this function
    #       ends, 'localize' is deallocated anyay.
    #
    #       Also this way, 'localize' & privileged can be used on the next functions quickly.
    #
    def localize(f):
        return Function(
                   function_code(f),
                   CapitalShared_scope,
                   intern_string(function_name(f)),
                   function_defaults(f),
                   function_closure(f),
               )


    def privileged(f):
        return Function(
                   function_code(f),
                   CapitalPrivileged_scope,
                   intern_string(function_name(f)),
                   function_defaults(f),
                   function_closure(f),
               )


    #
    #   'privileged' is currently privileged ... but it won't be once exported.
    #
    #       Hence make it permenantly privileged by calling it on itself :)
    #
    privileged = privileged(privileged)


    localize                 = privileged(localize)
    localize3_or_privileged2 = (privileged   if is_python_2 else   localize)


    #
    #   rename_code
    #
    if python_debug_mode:
        if is_python_2:
            @localize
            def rename_code(code, interned_name):
                return Code(
                           code_argument_count   (code),
                           code_number_locals    (code),
                           code_stack_size       (code),
                           code_flags            (code),
                           code_virtual_code     (code),
                           code_constants        (code),
                           code_global_names     (code),
                           code_variable_names   (code),
                           code_filename         (code),
                           interned_name,                           #   Rename to 'name'
                           code_first_line_number(code),
                           code_line_number_table(code),
                           code_free_variables   (code),
                           code_cell_vars        (code),
                      )
        else:
            @localize
            def rename_code(code, interned_name):
                return Code(
                           code_argument_count             (code),
                           code_keyword_only_argument_count(code),
                           code_number_locals              (code),
                           code_stack_size                 (code),
                           code_flags                      (code),
                           code_virtual_code               (code),
                           code_constants                  (code),
                           code_global_names               (code),
                           code_variable_names             (code),
                           code_filename                   (code),
                           interned_name,                           #   Rename to 'name'
                           code_first_line_number          (code),
                           code_line_number_table          (code),
                           code_free_variables             (code),
                           code_cell_vars                  (code),
                      )


    #
    #   rename_function
    #
    if python_debug_mode:
        @localize3_or_privileged2
        def rename_function(actual_name, f, code = none, scope = none):
            interned_name = intern_string(actual_name)

            return Function(
                       (code) or (rename_code(function_code(f), interned_name)),
                       (scope) or (function_globals(f)),
                       interned_name,
                       function_defaults(f),
                       function_closure(f),
                   )
    else:
        @localize3_or_privileged2
        def rename_function(actual_name, f, code = none, scope = none):
            if code is scope is none:
                return f

            return Function(
                       (code) or (function_code(f)),
                       (scope) or (function_globals(f)),
                       intern_string(actual_name),
                       function_defaults(f),
                       function_closure(f),
                   )


    #
    #   rename
    #
    if python_debug_mode:
        @localize
        def rename(format, *arguments):
            def rename(f):
                return rename_function((format % arguments   if arguments else   format), f)

            return rename
    else:
        @localize
        def rename(format, *arguments):
            def rename(f):
                return f

            return rename


    #
    #   next_method
    #       Access the .next method of an iterator
    #
    #       (Deals with the annoyance of .next method named .next in python 2.0, but .__next__ in python 3.0)
    #
    if is_python_2:
        @localize
        def next_method(iterator):
            return iterator.next
    else:
        @localize
        def next_method(iterator):
            return iterator.__next__


    #
    #   export
    #       Exports a function to Capital (Common Application Programming Interface Transcending All Languages);
    #       also the actual function exported is a copy of the original function -- but with its global scope
    #       replaced to `scope`.
    #
    #       Can also be used with multiple arguments to export a list of values (no replacement of
    #       global scope's is done in this case).
    #
    @localize
    def produce_actual_export(scope, insert):
        @localize3_or_privileged2
        def export(f, *arguments):
            if length(arguments) is 0:
                if (f.__class__ is Function) and (function_globals(f) is not CapitalPrivileged_scope):
                    name = intern_string(function_name(f))

                    return insert(
                               name,
                               Function(
                                   function_code(f),
                                   scope,                   #   Replace global scope with module's scope
                                   name,
                                   function_defaults(f),
                                   function_closure(f),
                               ),
                           )

                return insert(intern_string(f.__name__), f)

            argument_iterator = iterate(arguments)
            next_argument     = next_method(argument_iterator)

            insert(intern_string(f), next_argument())

            for name in argument_iterator:
                insert(intern_string(name), next_argument())


        return export


    @localize
    def produce_actual_transport(insert):
        @localize
        def transport(module_name, name):
            #debug('transport: %s', module_name)

            module     = require_module(module_name)
            module_map = module.__dict__

            insert(name, module_map[name])


        return transport


    #
    #   share_name & share_code
    #
    share_name = intern_string('share')


    if python_debug_mode:
        share_code = rename_code(function_code(produce_actual_export(0, 0)), share_name)


    #
    #   arrange
    #
    @localize
    def arrange(format, *arguments):
        return format % arguments


    #
    #   ThreadContext
    #
    if is_python_3:
        @localize
        def raising_exception(e):
            assert e.__cause__             is none
            assert (e.__context__ is none) or (is_instance(type(e.__context__), Exception))
            assert e.__suppress_context__  is False
            assert e.__traceback__         is none


        @localize
        def raising_exception_from(e, cause):
            assert e.__cause__             is none
            assert (e.__context__ is none) or (is_instance(type(e.__context__), Exception))
            assert e.__suppress_context__  is False
            assert e.__traceback__         is none

            e.__cause__ = cause
    else:
        ThreadingLocalBase = __import__('threading').local


        class ThreadContext(ThreadingLocalBase):
            __slots__ = (())


            def __init__(t):
                t.exception_stack = []
                t.last_exception  = none


        thread_context = ThreadContext()


        #
        #   Do not use 'hasattr' or 'getattr' here for multiple reasons:
        #
        #       1.  Mainly is defective (in that it can hide underlying errors in user functions);
        #       2.  Don't want to call user functions; and
        #       3.  Don't want to handle the complexity of extra exceptions here.
        #
        @localize
        def raising_exception(e):
            if '__cause__' in e.__dict__:
                assert '__context__'          in e.__dict__
                assert '__suppress_context__' in e.__dict__
                assert '__traceback__'        in e.__dict__
            else:
                assert '__context__'          not in e.__dict__
                assert '__suppress_context__' not in e.__dict__
                assert '__traceback__'        not in e.__dict__

                last_exception = thread_context.last_exception

                e.__cause__            = none
                e.__context__          = (none   if e is last_exception else   last_exception)
                e.__suppress_context__ = false
                e.__traceback__        = none


        @localize
        def raising_exception_from(e, cause):
            if '__cause__' in e.__dict__:
                assert '__context__'          in e.__dict__
                assert '__suppress_context__' in e.__dict__
                assert '__traceback__'        in e.__dict__
            else:
                assert '__context__'          not in e.__dict__
                assert '__suppress_context__' not in e.__dict__
                assert '__traceback__'        not in e.__dict__

                last_exception = thread_context.last_exception

                e.__context__          = (none if (e is last_exception) or (cause is last_exception) else   last_exception)
                e.__suppress_context__ = true
                e.__traceback__        = none

            e.__cause__ = cause


    #
    #   raise_already_exists
    #
    if python_debug_mode:
        NameError = Python_Exception.NameError


        @localize
        def raise_already_exists(module_name, name, previous, exporting):
            name_error = NameError(
                             arrange("%s.%s already exists (value: %r): can't export %r also",
                                     module_name, name, previous, exporting),
                         )

            raising_exception(name_error)

            #
            #   Since the next line will appear in stack traces, make it look prettier by using 'name_error'
            #   (to make the line shorter & more readable)
            #
            raise name_error


    #
    #   produce_dual_insert
    #
    if python_debug_mode:
        @localize
        def produce_dual_insert(actual_name, single_insert, provide, module_name):
            module_name = intern_string(module_name)


            @rename(actual_name)
            def dual_insert(name, exporting):
                previous = provide(name, single_insert(name, exporting))

                if previous is exporting:
                    return exporting

                raise_already_exists(module_name, name, previous, exporting)


            return dual_insert
    else:
        @localize
        def produce_dual_insert(actual_name, single_insert, provide, module_name):
            def dual_insert(name, exporting):
                return provide(name, single_insert(name, exporting))


            return dual_insert


    #
    #   produce_single_insert
    #
    if python_debug_mode:
        @localize
        def produce_single_insert(actual_name, provide, module_name):
            module_name = intern_string(module_name)


            @rename(actual_name)
            def single_insert(name, exporting):
                previous = provide(name, exporting)

                if previous is exporting:
                    return previous

                raise_already_exists(module_name, name, previous, exporting)


            return single_insert
    else:
        @localize
        def produce_single_insert(actual_name, provide, module_name):
            return provide


    #
    #   built_in & restricted
    #
    insert_privileged = produce_single_insert(
                            'insert_privileged',
                             CapitalPrivileged_scope.setdefault,
                             CapitalPrivileged.__name__,
                        )

    insert__built_in = produce_dual_insert(
                           'insert__built_in',
                           insert_privileged,
                           CapitalBuiltIn_scope.setdefault,
                           CapitalBuiltIn.__name__,
                       )

    built_in   = produce_actual_export(CapitalShared_scope, insert__built_in)
    restricted = produce_actual_export(CapitalShared_scope, insert_privileged)


    if python_debug_mode:
        built_in   = rename_function('built_in',   built_in)
        restricted = rename_function('restricted', restricted)


    #
    #   special_builtins_name
    #
    special_builtins_name  = intern_string('__builtins__')


    #
    #   produce_export_transport_and_share
    #
    interned_Shared_name = intern_string('Shared')


    @localize
    def produce_export_transport_and_share(module, Shared = none):
        module_name  = module.__name__ = intern_string(module.__name__)
        module_scope = module.__dict__

        if Shared is none:
            Shared_name = intern_string(arrange('%s.Shared', module_name))
            Shared      = Module(Shared_name)
        else:
            Shared_name = Shared.__name__

            assert Shared_name is intern_string(Shared_name)

        Shared_scope = Shared.__dict__

        insert_share = produce_single_insert(
                           'insert_share',
                           Shared_scope.setdefault,
                           Shared_name,
                       )

        insert_export = produce_dual_insert(
                            'insert_export',
                            insert_share,
                            module_scope.setdefault,
                            module_name,
                        )

        export    = produce_actual_export   (Shared_scope, insert_export)
        transport = produce_actual_transport(insert_share)
        share     = produce_actual_export   (Shared_scope, insert_share)

        if python_debug_mode:
            share = rename_function(share_name, share, code = share_code)

        share(
                special_builtins_name,  CapitalBuiltIn_scope,
                'export',               export,
                'transport',            transport,
                share_name,             share,
                interned_Shared_name,   Shared,
            )

        export(
                interned_Shared_name,   Shared,
            )

        store_python_module(Shared_name, Shared)


    #
    #   export & share
    #
    produce_export_transport_and_share(Capital, Shared = CapitalShared)

    export = CapitalShared.export
    share  = CapitalShared.share


    #
    #   Only put in CapitalBuiltIn_scope: __build__class & __import__
    #
    #       (not needed in CapitalPrivileged_scope, since it is found in CapitalPrivileged_scope['__builtins__'])
    #
    if is_python_3:
        CapitalBuiltIn_scope[intern_string(Python_BuiltIn.__build_class__.__name__)] = Python_BuiltIn.__build_class__


    CapitalBuiltIn_scope[intern_string(Python_BuiltIn.__import__.__name__)] = Python_BuiltIn.__import__


    #
    #   Privileged
    #       Put the Python BuiltIn module into CapitalPrivileged: making it an unrestricted scope.
    #
    restricted(
        special_builtins_name, Python_BuiltIn.__dict__,
    )


    #
    #   Initial shares's
    #
    share(
        #
        #   __builtins__
        #
        #'__builtins__',    CapitalBuiltIn_scope,                       #   Done in produce_export_transport_and_share
        #

        #   Functions
        #
        'built_in',     built_in,
        'restricted',   restricted,

        #
        #   Modules
        #
        'Privileged',           CapitalPrivileged,
        'Python_BuiltIn',       Python_BuiltIn,
        'Python_System',        Python_System,
        'Python_Exception',     Python_Exception,
        #'Shared',              Shared                                  #   Done in produce_export_transport_and_share
    )


    if is_python_2:
        share(
            'thread_context',     thread_context,
        )


    export(
        #
        #   Keywords
        #       implemented as keywords in Python 3.0 -- so can't use an expression like 'Python_BuiltIn.None'.
        #
        'false',    false,
        'none',     none,
        'true',     true,


        #
        #   Modules
        #
        'BuiltIn',  CapitalBuiltIn,
        #'Shared',  CapitalShared,                                      #   Done in produce_export_transport_and_share


        #
        #   Types
        #
        'Module',       Module,
        'String',       String,
        'Traceback',    Traceback,


        #
        #   Functions
        #
        'arrange',                  arrange,
        'intern_string',            intern_string,
        'is_instance',              is_instance,
        'length',                   length,
        'next_method',              next_method,
        'privileged',               privileged,
        'raising_exception',        raising_exception,
        'raising_exception_from',   raising_exception_from,
        'rename_function',          rename_function,
        'rename',                   rename,


        #
        #   Values
        #
        'is_python_2',          is_python_2,
        'is_python_3',          is_python_3,
        'python_debug_mode',    python_debug_mode,
    )


    #
    #   Main
    #
    Main = python_modules['__main__']


    #
    #   capital_modules (also lookup_capital_module & store_capital_module)
    #
    capital_modules       = { Capital_name : Capital }
    lookup_capital_module = capital_modules.get
    store_capital_module  = capital_modules.__setitem__


    #
    #   module_annotation
    #
    module_annotation_name = intern_string('module')


    @localize3_or_privileged2
    def module_annotation(module_name):
        #debug('module: %r', module_name)

        dot_index = module_name.rfind('.')

        if dot_index == -1:
            parent_module_name = module_name
            child_module_name  = module_name
        else:
            parent_module_name = module_name[:dot_index]
            child_module_name  = module_name[dot_index + 1:]

        parent_module = lookup_capital_module(parent_module_name)

        if parent_module is none:
            assert dot_index != -1

            parent_module = require_module(parent_module_name)

        Shared_Scope = parent_module.Shared.__dict__

        #debug('child_module_name: %r', child_module_name)

        if child_module_name == 'Main':
            del Main.module


            @privileged
            def execute(f):
                assert f.__name__ == module_annotation_name

                Function(
                    function_code(f),
                    Shared_Scope,            #   Replace global scope with the module's shared scope
                    module_annotation_name,
                    function_defaults(f),
                    function_closure(f),
                )(
                )

                arguments = Python_System.argv[1:]
                main      = parent_module.Shared.__dict__.pop('main')

                main(arguments)


            return execute


        @privileged
        def execute(f):
            assert f.__name__ == module_annotation_name

            #debug('start: %r', module_name)

            Function(
                function_code(f),
                Shared_Scope,            #   Replace global scope with the module's shared scope
                module_annotation_name,
                function_defaults(f),
                function_closure(f),
            )(
            )

            #debug('done: %r', module_name)
            return module_annotation


        return execute


    #
    #   fast_cache
    #
    fast_cache = module_annotation_scope.get('fast_cache', 0)


    if fast_cache is not 0:
        del module_annotation_scope['fast_cache']

        fast_pop = fast_cache.pop

        share(
            'produce_export_transport_and_share',   produce_export_transport_and_share,
            'store_capital_module',                 store_capital_module,
        )
    else:
        @localize
        def fast_pop(module_name, alternate):
            return alternate


    #
    #   require_module
    #
    if is_python_2:
        #
        #   Python 2.* method of loading a module with `module` pre-initialized
        #
        #       This is messy -- see below for the Python 3.0 method which is much cleaner.
        #
        Python_OldImport  = __import__('imp')
        find_module       = Python_OldImport.find_module
        load_module       = Python_OldImport.load_module
        PACKAGE_DIRECTORY = Python_OldImport.PKG_DIRECTORY


        @built_in
        @privileged
        def require_module(module_name):
            module = lookup_capital_module(module_name)

            if module is not none:
                return module

            #debug('require_module: %r', module_name)

            fast = fast_pop(module_name, 0)

            dot_index = module_name.rfind('.')

            if dot_index is not -1:
                parent_module = require_module(module_name[:dot_index])

            module_name = intern_string(module_name)
            module      = Module(module_name)

            is_package = 0

            #
            #   Temporarily store our module in 'python_modules[module_name]'.
            #
            #   This is needed in python 2.*, as the way to pass the 'pre-initialized' module to 'load_module'
            #
            #       (In the cleaner python 3.* version below, we pass the modules directly to 'exec_module'
            #       and thus do not need to store the module in 'python_modules[module_name]').
            #
            #   NOTE:
            #       If this was real import implementation we would need to cleanup
            #       'python_modules[module_name]' When an exception is thrown.
            #
            #       However, this is not a true import mechanism.  If it fails, our program will simply
            #       exit.
            #
            #       Therefore, there is no 'try' clause below to cleanup if 'load_module' throws ImportError
            #
            result = (module   if dot_index is -1 else   parent_module)

            store_capital_module(module_name, result)                   #   `result` on purpose
            store_python_module(module_name, module)

            if fast is not 0:
                #debug('fast processing %s', module_name)

                if dot_index == -1:
                    produce_export_transport_and_share(module)

                module_annotation(module_name)(fast)
            else:
                if fast_cache:
                    debug('slow processing %s', module_name)

                if dot_index is -1:
                    [f, pathname, description] = find_module(module_name)
                else:
                    [f, pathname, description] = find_module(module_name[dot_index + 1:], parent_module.__path__)

                #debug('%r: %r, %r, %r', module_name, f, pathname, description)

                module.module = module_annotation

                #
                #   CAREFUL here:
                #       We *MUST* close 'f' if any exception is thrown.
                #
                #       So ASAP use 'f' within a 'with' clause (this ensures 'f' is always closed, whether
                #       an exception is thrown or not)
                #
                if f is not none:
                    with f:
                        load_module(module_name, f, pathname, description)
                else:
                    if description[2] == PACKAGE_DIRECTORY:
                        is_package = 7
                        produce_export_transport_and_share(module)

                    load_module(module_name, f, pathname, description)

            #
            #   If this is a package: Keep this module
            #   Otherwise:            Discard this module (it is no longer needed)
            #
            if is_package is not 7:
                del python_modules[module_name]

            return result
    else:
        #
        #   Python 3.* method of loading a module with `module` pre-initialized
        #
        Python_ImportUtility         = __import__('importlib.util').util
        ImportError                  = Python_BuiltIn.ImportError
        lookup_module_blueprint      = Python_ImportUtility.find_spec
        create_module_from_blueprint = Python_ImportUtility.module_from_spec


        @built_in
        def require_module(module_name):
            module = lookup_capital_module(module_name)

            if module is not none:
                return module

            #debug('require_module: %r', module_name)

            fast = fast_pop(module_name, 0)

            dot_index = module_name.rfind('.')

            if dot_index is not -1:
                parent_module = require_module(module_name[:dot_index])

            module_name = intern_string(module_name)

            if fast is not 0:
                #debug('fast processing %s', module_name)

                module = Module(module_name)
                result = (module   if dot_index is -1 else   parent_module)

                if dot_index == -1:
                    produce_export_transport_and_share(module)

                store_capital_module(module_name, result)               #   `result` on purpose
                store_python_module(module_name, module)

                module_annotation(module_name)(fast)

                result = (module   if dot_index is -1 else   parent_module)

                if '__path__' not in module.__dict__:
                    delete_python_module(module_name)

                return result


            if fast_cache:
                debug('slow processing %s', module_name)

            blueprint = lookup_module_blueprint(module_name)

            if blueprint is none:
                import_error = ImportError(arrange("Can't find module %s", module_name))

                raising_exception(import_error)

                #
                #   Since the next line will appear in stack traces, make it look prettier by using 'import_error'
                #   (to make the line shorter & more readable)
                #
                raise import_error

            is_package = 0
            module     = create_module_from_blueprint(blueprint)
            result     = (module   if dot_index is -1 else   parent_module)

            store_capital_module(module_name, result)

            if '__path__' in module.__dict__:
                is_package = 7
                produce_export_transport_and_share(module)

            module.module = module_annotation

            #debug('exec_module: %s', module_name)

            blueprint.loader.exec_module(module)

            #
            #   If this is a package: Keep this module
            #   Otherwise:            Discard this module (it is no longer needed)
            #
            if is_package is 7:
                store_python_module(module_name, module)

            return result


    require_module('Capital.Core')


    #
    #   main
    #
    Capital.boot = boot
    Main.module = module_annotation


    if fast_cache is 0:
        del Capital.__builtins__
        del Capital.__package__


    built_in('fast_cache', fast_cache)
