#
#  Copyright (c) 2018 Joy Diamond.  All rights reserved.
#
@module('Capital.Capital_Object')
def module():
    require_module('Capital.ObjectMembers')


    #
    #   PREPARE__cannot_be_compared__ERROR
    #
    def PREPARE__cannot_be_compared__ERROR(t, function_name):
        return PREPARE__instances_of_type__X__cannot__Y__ERROR(t, function_name, 'be compared')


    #
    #   PREPARE__class__X__cannot__Y__ERROR
    #   PREPARE__instances_of_type__X__are_immutable_and_cannot__Y__ERROR
    #   PREPARE__instances_of_type__X__cannot_be_allocated_with_new__ERROR
    #   PREPARE__instances_of_type__X__cannot__Y__ERROR
    #   PREPARE__invalid_function__use__Y__instead__ERROR
    #
    @share
    def PREPARE__class__X__cannot__Y__ERROR(m, function_name, description):
        m_name = m.__name__

        return PREPARE_attribute_ERROR("`%s.%s`: class `%s` cannot %s",
                                       m_name, function_name, m_name, description)


    def PREPARE__instances_of_type__X__are_immutable_and_cannot__Y__ERROR(t, function_name, description):
        type_name = type(t).__name__

        return PREPARE_attribute_ERROR("`%s.%s`: instances of type `%s` are immutable and cannot %s",
                                       type_name, function_name, type_name, description)


    def PREPARE__instances_of_type__X__cannot_be_allocated_with_new__ERROR(m):
        #
        #   This is a special version (only used by operator new (__new__)) to
        #   include the phrase "instance of type" even though the first argument is a class
        #   (instead of an instance).
        #
        m_name = m.__name__

        return PREPARE_attribute_ERROR("`%s.%s`: instances of type `%s` cannot %s",
                                       m_name, 'operator new (__new__)', m_name, 'be allocated with `new`')


    def PREPARE__instances_of_type__X__cannot__Y__ERROR(t, function_name, description):
        type_name = type(t).__name__

        return PREPARE_attribute_ERROR("`%s.%s`: instances of type `%s` cannot %s",
                                       type_name, function_name, type_name, description)


    #
    #   operator compare
    #   operator =
    #   operator !=
    #   operator >
    #   operator >=
    #   operator <
    #   operator <=
    #
    #   NOTE:
    #       Cannot use `@property` on these compare operators, as python (2 & 3) do not
    #       call these as functions if they are properties (when comparing).
    #
    if is_python_2:
        @share
        def operator__compare__NOT_ALLOWED(t, _that):
            attribute_error = PREPARE__cannot_be_compared__ERROR(t, 'operator compare (__cmp__)')

            raise attribute_error


    @share
    def operator__equal__NOT_ALLOWED(t, _that):
        attribute_error = PREPARE__cannot_be_compared__ERROR(t, 'operator == (__eq__)')

        raise attribute_error


    @share
    def operator__greater_than__NOT_ALLOWED(t, _that):
        attribute_error = PREPARE__cannot_be_compared__ERROR(t, 'operator > (__gt__)')

        raise attribute_error


    @share
    def operator__greater_than_or_equal__NOT_ALLOWED(t, _that):
        attribute_error = PREPARE__cannot_be_compared__ERROR(t, 'operator >= (__ge__)')

        raise attribute_error


    @share
    def operator__less_than__NOT_ALLOWED(t, _that):
        attribute_error = PREPARE__cannot_be_compared__ERROR(t, 'operator < (__lt__)')

        raise attribute_error


    @share
    def operator__less_than_or_equal__NOT_ALLOWED(t, _that):
        attribute_error = PREPARE__cannot_be_compared__ERROR(t, 'operator <= (__le__)')

        raise attribute_error


    @share
    def operator__not_equal__NOT_ALLOWED(t, _that):
        attribute_error = PREPARE__cannot_be_compared__ERROR(t, 'operator != (__ne__)')

        raise attribute_error


    #
    #   raise__CANNOT__create__ERROR
    #   raise__CANNOT__construct__ERROR
    #
    @class_method
    def raise__CANNOT__create__ERROR(m, *arguments, **keywords):
        attribute_error = PREPARE__instances_of_type__X__cannot_be_allocated_with_new__ERROR(m)

        raise attribute_error


    #@property
    def raise__CANNOT__construct__ERROR(t, *arguments, **keywords):
        attribute_error = PREPARE__instances_of_type__X__cannot__Y__ERROR(
                t,
                'operator constructor (__init__)',
                'be constructed'
            )

        raise attribute_error


    #
    #   raise__CANNOT__{delete,set}_attribute__ERROR
    #
    @property
    def raise__CANNOT__delete_attribute__ERROR(t):
        attribute_error = PREPARE__instances_of_type__X__are_immutable_and_cannot__Y__ERROR(
                t,
                'operator delete attribute (__delattr__)',
                'delete attributes'
            )

        raise attribute_error


    @property
    def raise__CANNOT__set_attribute__ERROR(t):
        attribute_error = PREPARE__instances_of_type__X__are_immutable_and_cannot__Y__ERROR(
                t,
                'operator set attribute (__setattr__)',
                'set attributes'
            )

        raise attribute_error


    #
    #   raise__CANNOT__format__ERROR
    #
    @property
    def raise__CANNOT__format__ERROR(t):
        attribute_error = PREPARE__instances_of_type__X__cannot__Y__ERROR(
                t,
                'operator format (__format__)',
                'be formatted inside a string (using `format`)'
            )

        raise attribute_error


    #
    #   raise__CANNOT__introspection__ERROR
    #
    def raise__CANNOT__introspection__ERROR(t):
        attribute_error = PREPARE__instances_of_type__X__cannot__Y__ERROR(
                t,
                'operator introspect (__dir__)',
                'be introspected',
            )

        raise attribute_error


    #
    #   raise__CANNOT__reduce__ERROR
    #   raise__CANNOT__reduce_extended__ERROR
    #
    #   NOTE:
    #       Cannot use `@property` on these reduce operators, as python (2 & 3) do not
    #       call these as functions if they are properties (when pickling).
    #
    @share
    def raise__CANNOT__reduce__ERROR(t):
        attribute_error = PREPARE__instances_of_type__X__cannot__Y__ERROR(
                t,
                'operator reduce (__reduce__)',
                'be pickeled',
            )

        raise attribute_error


    @share
    def raise__CANNOT__reduce_extended__ERROR(t, protocol):
        attribute_error = PREPARE__instances_of_type__X__cannot__Y__ERROR(
                t,
                'operator reduce extended (__reduce_ex__)',
                'be pickeled',
            )

        raise attribute_error


    #
    #   raise__CANNOT__subclass_hook__ERROR
    #
    @class_method
    def raise__CANNOT__subclass_hook__ERROR(m, subclass):
        attribute_error = PREPARE__class__X__cannot__Y__ERROR(
                m,
                'operator sub class hook (__subclasshook__)',
                'test for sub class',
            )

        raise attribute_error


    #
    #   All `Object` members (except compare operators)
    #
    hidden_introspection_keys = {
        #
        #   Empty slots
        #
        '__slots__'        : (()),


        #
        #   Most `Object` members
        #
        '__class__'        : Object__member__class_type,
        '__delattr__'      : raise__CANNOT__delete_attribute__ERROR,
        '__doc__'          : "base of all Capital Objects",
        '__format__'       : raise__CANNOT__format__ERROR,
        '__getattribute__' : Object__operator__get_attribute,                         #   tp_getattr
        '__hash__'         : none,                                                    #   tp_hash
        '__init__'         : raise__CANNOT__construct__ERROR,                         #   tp_init
        '__new__'          : raise__CANNOT__create__ERROR,                            #   tp_new
        '__reduce__'       : raise__CANNOT__reduce__ERROR,
        '__reduce_ex__'    : raise__CANNOT__reduce_extended__ERROR,
        '__repr__'         : Object__operator__representation,                        #   tp_repr
        '__setattr__'      : raise__CANNOT__set_attribute__ERROR,                     #   tp_setattr
        '__sizeof__'       : Object__operator__sizeof,
        '__str__'          : Object__operator__string,                                #   tp_str
        '__subclasshook__' : raise__CANNOT__subclass_hook__ERROR,

        #
        #   NOTE:
        #       Although `Object.__dir__` is *ONLY* defined in python 3:
        #
        #           `__dir__` is called by *BOTH* python 2 & 3, if it exists.
        #
        #       Therefore, we declare in *BOTH* python 2 & 3.
        #
        '__dir__' : raise__CANNOT__introspection__ERROR,

        #
        #   New names
        #
        'class_type' : Object__member__class_type,
    }


    def produce_Capital_Object(name, documentation):
        hidden_introspection_keys['__doc__'] = documentation

        r = Type(name, ((Object,)), hidden_introspection_keys)

        del hidden_introspection_keys['__doc__']

        del r.__slots__

        return r


    Comparable_Object = produce_Capital_Object('Comparable_Object', "base of all comparable Capital objects")


    if is_python_2:
        hidden_introspection_keys['__cmp__'] = operator__compare__NOT_ALLOWED

    hidden_introspection_keys['__eq__'] = operator__equal__NOT_ALLOWED
    hidden_introspection_keys['__gt__'] = operator__greater_than__NOT_ALLOWED
    hidden_introspection_keys['__ge__'] = operator__greater_than_or_equal__NOT_ALLOWED
    hidden_introspection_keys['__le__'] = operator__less_than_or_equal__NOT_ALLOWED
    hidden_introspection_keys['__lt__'] = operator__less_than__NOT_ALLOWED
    hidden_introspection_keys['__ne__'] = operator__not_equal__NOT_ALLOWED


    Capital_Object = produce_Capital_Object('Capital_Object', "base of all non-comparale Capital objects")


    #
    #   ignore_introspection_key
    #
    #   The following keys are hidden:
    #
    #       __class__                   #   Use `.class_type` instead (hence we hide `.__class__`)
    #       __delattr__
    #       __format__
    #       __init__
    #       __new__
    #       __reduce__
    #       __reduce_ex__
    #       __setattr__
    #       __subclasshook__
    #
    #       __dir__
    #
    #       __cmp__                     #   Python 2 only
    #       __eq__
    #       __ge__
    #       __gt__
    #       __le__
    #       __lt__
    #       __ne__
    #
    del hidden_introspection_keys['__slots__']
    del hidden_introspection_keys['__getattribute__']
    del hidden_introspection_keys['__hash__']
    del hidden_introspection_keys['__repr__']
    del hidden_introspection_keys['__sizeof__']
    del hidden_introspection_keys['__str__']
    del hidden_introspection_keys['class_type']


    lookup__hidden_introspection_key = hidden_introspection_keys.get


    #
    #   NOTE:
    #       For python 3:
    #
    #           We need to call `Object.__new__` but without extra parameters.
    #
    #           This avoids the error:
    #
    #               TypeError: object() takes no parameters
    #
    #           See "UnitTest/Class.py" function `test_operator_new` which test for this.
    #
    if is_python_2:
        Capital_Object__operator_new = new_instance
    else:
        #
        #   NOTE #2:
        #       Cannot use `@export` here, since `staticmethod` object has no attribute `.__name`
        #
        @static_method
        def Capital_Object__operator_new(m, *arguments, **keyword):
            return new_instance(m)


    #
    #   base_classes__Capital_Object
    #
    base_classes__Capital_Object = ((Capital_Object,))



    #
    #   share & export
    #
    share(
            'hidden_introspection_keys',            hidden_introspection_keys,
            'raise__CANNOT__construct__ERROR',      raise__CANNOT__construct__ERROR,
            'raise__CANNOT__create__ERROR',         raise__CANNOT__create__ERROR,
            'raise__CANNOT__format__ERROR',         raise__CANNOT__format__ERROR,
            'raise__CANNOT__subclass_hook__ERROR',  raise__CANNOT__subclass_hook__ERROR,
        )


    export(
            'base_classes__Capital_Object',         base_classes__Capital_Object,
            'Capital_Object',                       Capital_Object,
            'Capital_Object__operator_new',         Capital_Object__operator_new,
            'Comparable_Object',                    Comparable_Object,
            'lookup__hidden_introspection_key',     lookup__hidden_introspection_key,
        )
