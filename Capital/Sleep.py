#
#   Copyright (c) 2018 Joy Diamond.  All rights reserved.
#
@module('Capital.Sleep')
def module():
    require_module('Capital.Import')


    Python_Time = import_module('time')


    export(
        'sleep',    Python_Time.sleep,
    )
