#
#   Copyright (c) 2017-2018 Joy Diamond.  All rights reserved.
#
@module('Capital.Codec')
def module():
    require_module('Capital.Import')


    Python_Codec        = import_module('codecs')
    python_encode_ascii = Python_Codec.getencoder('ascii')


    @export
    def encode_ascii(s):
        return python_encode_ascii(s)[0]
