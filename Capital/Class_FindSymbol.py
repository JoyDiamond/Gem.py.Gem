#
#  Copyright (c) 2018 Joy Diamond.  All rights reserved.
#
@module('Capital.Class_FindSymbol')
def module():
    show_reuse = 0


    require_module('Capital.Absent')


    #
    #   `Type__operator__class_members`:
    #
    #       Need to create our own version of `Type__operator__class_members` here, since `Capital.TypeMembers` is
    #       dependent on this file -- and we want to avoid creating an import loop (i.e.: imports dependent on each
    #       other).
    #
    Type__operator__class_members = Type.__dict__


    query__Type__method_resolution_order = Type__operator__class_members['__mro__'].__get__
    query__Type__name                    = Type__operator__class_members['__name__'].__get__
    query__Type__class_members           = Type__operator__class_members['__dict__'].__get__



    #
    #<conjure__class_members>
    #
    cache__class_members   = {}
    lookup__class_members  = cache__class_members.get
    provide__class_members = cache__class_members.setdefault


    @export
    def conjure__class_members(m):
        return (lookup__class_members(m)) or (provide__class_members(m, query__Type__class_members(m)))
    #</conjure__class_members>


    #
    #<conjure__class_members__keys>
    #
    cache__class_members__keys   = {}
    lookup__class_members__keys  = cache__class_members__keys.get
    provide__class_members__keys = cache__class_members__keys.setdefault


    @share
    def conjure__class_members__keys(m):
        return (
                      lookup__class_members__keys(m)
                   or provide__class_members__keys(m, query__Type__class_members(m).keys)
               )
    #</conjure__class_members__keys>


    #
    #<conjure__lookup__class_member>
    #
    cache__lookup__class_member   = { Type : Type__operator__class_members.get }
    lookup__lookup__class_member  = cache__lookup__class_member.get
    provide__lookup__class_member = cache__lookup__class_member.setdefault


    def conjure__lookup__class_member(m_resolver):
        return (
                   lookup__lookup__class_member (m_resolver)
                or provide__lookup__class_member(m_resolver, conjure__class_members(m_resolver).get)
            )
    #</conjure__lookup__class_member>


    #
    #<produce__query_symbol>
    #
    #   "query" = "lookup" | "lookup_and_show" (depending on `show` parameter).
    #
    def produce__query_symbol(m, m_name, search, show, depth):
        lookup__class_member = conjure__lookup__class_member(search)

        if not show:
            return lookup__class_member

        def lookup_and_show_symbol(name, default):
            assert default is absent

            r = lookup__class_member(name, absent)

            if r is absent:
                line('find_symbol(`%s`): no symbol `%s.%s` (#%d)',
                     m_name,
                     query__Type__name(search),
                     name,
                     depth)
            else:
                if (depth == 1) and (m is search):
                    line('find_symbol(`%s`): found `.%s`: %r', m_name, name, r)
                else:
                    line('find_symbol(`%s`): found `%s.%s` (#%d): %r',
                         m_name,
                         query__Type__name(search),
                         name,
                         depth,
                         r)

            return r


        return lookup_and_show_symbol
    #</produce__query_symbol>


    #
    #<produce__find_symbol>
    #
    find_symbol_cache  = {}
    lookup_find_symbol = find_symbol_cache.get
    stash_find_symbol  = find_symbol_cache.setdefault


    @export
    def produce__find_symbol(m, show = false):
        search_tuple = query__Type__method_resolution_order(m)

        previous = lookup_find_symbol(search_tuple)

        if previous is not none:
            if show_reuse:
                line('REUSE for `%s`: `%s`', query__Type__name(m), previous.__name__)

            return previous

        search_maximum = length(search_tuple)
        m_name         = query__Type__name(m)


        def raise_CANNOT_find_symbol(name):
            raise_runtime_error('cannot find symbol `%s` in class `%s`', name, m_name)

        #
        #<depth 1>
        #
        m1             = search_tuple[0]
        query_symbol_1 = produce__query_symbol(m, m_name, m1, show, 1)

        if search_maximum == 1:
            @rename('find_symbol_1__%s', m_name)
            def find_symbol_1(name):
                r = query_symbol_1(name, absent)
                if r is not absent:
                    return r

                raise_CANNOT_find_symbol(name)


            return stash_find_symbol(search_tuple, find_symbol_1)
        #</depth>

        #
        #<depth 2>
        #
        m2             = search_tuple[1]
        query_symbol_2 = produce__query_symbol(m, m_name, m2, show, 2)

        if search_maximum == 2:
            @rename('find_symbol_2__%s', m_name)
            def find_symbol_2(name):
                r = query_symbol_1(name, absent)
                if r is not absent:
                    return r

                r = query_symbol_2(name, absent)
                if r is not absent:
                    return r

                raise_CANNOT_find_symbol(name)


            return stash_find_symbol(search_tuple, find_symbol_2)
        #</depth>

        #
        #<depth 3>
        #
        m3             = search_tuple[2]
        query_symbol_3 = produce__query_symbol(m, m_name, m3, show, 3)


        if search_maximum == 3:
            @rename('find_symbol_3__%s', m_name)
            def find_symbol_3(name):
                r = query_symbol_1(name, absent)
                if r is not absent:
                    return r

                r = query_symbol_2(name, absent)
                if r is not absent:
                    return r

                r = query_symbol_3(name, absent)
                if r is not absent:
                    return r

                raise_CANNOT_find_symbol(name)


            return stash_find_symbol(search_tuple, find_symbol_3)
        #</depth>

        #
        #<depth 4+>
        #
        query_symbol__many   = [query_symbol_1, query_symbol_2, query_symbol_3]
        append__query_symbol = query_symbol__many.append
        find__query_symbol   = query_symbol__many.__getitem__

        depth = 3

        while depth < search_maximum:
            search = search_tuple[depth]

            depth += 1

            append__query_symbol(produce__query_symbol(m, m_name, search, show, depth))


        query_symbol__maximum = length(query_symbol__many)

        assert query_symbol__maximum == search_maximum


        @rename('find_symbol_7__%s', m_name)
        def find_symbol_7(name):
            i = 0

            while i < query_symbol__maximum:
                query_symbol = find__query_symbol(i)

                r = query_symbol(name, absent)
                if r is not absent:
                    return r

                i += 1

            raise_CANNOT_find_symbol(name)


        return stash_find_symbol(search_tuple, find_symbol_7)
        #</depth>


    #
    #   find_symbol_base_and_depth
    #
    @export
    def find_symbol_base_and_depth(m, name):
        depth = 1

        for search in query__Type__method_resolution_order(m):
            lookup__class_member = conjure__lookup__class_member(search)

            r = lookup__class_member(name, absent)
            if r is not absent:
                return ((r, search, depth))

            depth += 1

        raise_runtime_error('cannot find symbol `%s` in class `%s`', name, query__Type__name(m))


    #
    #   share
    #
    share(
            'query__Type__method_resolution_order',     query__Type__method_resolution_order,
        )
