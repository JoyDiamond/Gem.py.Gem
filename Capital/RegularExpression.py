#
#   Copyright (c) 2017-2018 Joy Diamond.  All rights reserved.
#
@module('Capital.RegularExpression')
def module():
    require_module('Capital.Import')


    Python_RegularExpression          = import_module('re')
    compile_python_regular_expression = Python_RegularExpression.compile


    @export
    def make_match_function(pattern):
        return compile_python_regular_expression(pattern).match
