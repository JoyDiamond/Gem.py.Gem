#
#  Copyright (c) 2018 Joy Diamond.  All rights reserved.
#
@module('Capital.ProcessObjectMembers')
def module():
    #
    #   The base classes for a class derived from `Object`
    #
    base_classes__Object = ((Object,))


    #
    #   Reserved member names
    #
    reserved_member_names = FrozenSet([
        #
        #   From `Object`
        #
        '__class__',            'class_type',
        '__delattr__',          'operator__delete_attribute',
        '__doc__',              'class_documentation',
        '__format__',           'operator__format',
        '__getattribute__',     'operator__get_attribute',          #   tp_getattr
        '__hash__',             'operator__hash',                   #   tp_hash
        '__init__',             'constructor',                      #   tp_init
        '__new__',              'operator__new',                    #   tp_new
        '__reduce__',           'operator__reduce',
        '__reduce_ex__',        'operator__reduce_extended',
        '__repr__',             'operator__representation',         #   tp_repr
        '__setattr__',          'operator__set_attribute',          #   tp_setattr
        '__sizeof__',           'instance__size',
        '__str__',              'operator__string',                 #   tp_str
        '__subclasshook__',     'operator__subclass_hook'

        #
        #   From `Object` (Python 3)
        #
        '__dir__',              'operator__introspection',


        #
        #   Compare operators
        #
        '__eq__',               'operator__equal',
        '__ge__',               'operator__greater_than',
        '__gt__',               'operator__greater_than_or_equal',
        '__le__',               'operator__less_than',
        '__lt__',               'operator__less_than_or_equal',
        '__ne__',               'operator__not_equal',


        #
        #   From `Type`
        #
        '__abstractmethods__',  'class_abstract_methods',
        '__base__',             'class_parent',
        '__bases__',            'class_bases',
        '__basicsize__',        'class_basic_size',
        '__call__',             'operator__call',
        '__dict__',             'class_members',
        '__dictoffset__',       'class_members_offset',
        '__flags__',            'class_flags',
        '__instancecheck__',    'operator__instance_check',
        '__itemsize__',         'class_item_size',
        '__metaclass__',
        '__mro__',              'class_method_resolution_order',
        'mro',                  'class_calculate_method_resolution_order',
        '__name__',             'class_name',
        '__subclasscheck__',    'operator__subclass_check',
        '__subclasses__',       'operator__subclasses',
        '__weakrefoffset__',    'class_weak_reference_offset',


        #
        #   From `Type` (Python 3)
        #
        '__prepare__',          'operator__prepare',
        '__qualname__',         'class_qualified_name',
        '__text_signature__',   'class_text_signature',


        #
        #   Other
        #
        '__module__',           'class_module',
    ])



    @export
    def process_object_members(name, members, introspectable = true, newable = false):
        if newable:
            operator_new = members.pop('operator_new', Capital_Object__operator_new)

        if FrozenSet(members) & reserved_member_names:
            raise__reserved_names__error('process_object_members', name, members, reserved_member_names)

        provide  = members.setdefault
        contains = members.__contains__


        #
        #   Always provide a `slots` members, if missing
        #
        provide('__slots__', (()) )

        #
        #   All members from `Object` (except for `doc`)
        #
        provide('__class__',        Object__member__class_type)

        if newable:
            provide('__new__',      operator_new)                       #   tp_new

        if contains('portray'):
            provide('__repr__',     members['portray'])                 #   tp_repr

        if introspectable:
            if is_python_3:
                provide('__dir__',  Object__operator__introspection)


        #
        #   New names
        #
        provide('class_type',       Object__member__class_type)

        return members


    #
    #   share & export
    #
    share(
            'reserved_member_names',                reserved_member_names,
        )


    export(
            'base_classes__Object',                 base_classes__Object,
        )
