#
#   Copyright (c) 2017-2018 Joy Diamond.  All rights reserved.
#
@module('Capital.ExecuteFile')
def module():
    require_module('Capital.Path')


    from Capital import Module, path_basename, path_split_extension, read_text_from_path


    compile_python = Python_BuiltIn.compile
    execute_code   = Python_BuiltIn.eval


    @export
    def execute_python_from_file(path):
        path                  = intern_string(path)
        [basename, extension] = path_split_extension(path_basename(path))
        basename              = intern_string(basename)
        module                = Module(basename)

        execute_code(
            compile_python(read_text_from_path(path), path, 'exec'),
            module.__dict__,
        )

        return module
