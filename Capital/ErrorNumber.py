#
#   Copyright (c) 2017-2018 Joy Diamond.  All rights reserved.
#
@module('Capital.ErrorNumber')
def module():
    require_module('Capital.Import')


    Python_ErrorNumber = import_module('errno')


    export(
        'ERROR_NO_ACCESS',  Python_ErrorNumber.EACCES,
        'ERROR_NO_ENTRY',   Python_ErrorNumber.ENOENT,
    )
