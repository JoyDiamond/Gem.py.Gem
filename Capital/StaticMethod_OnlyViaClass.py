#
#   Copyright (c) 2017-2018 Joy Diamond.  All rights reserved.
#
@module('Capital.StaticMethod_OnlyViaClass')
def module():
    #
    #   StaticMethod_OnlyViaClass:
    #
    #       Same as `static_method`:
    #
    #           But throws an exception when accesssed via an instance of the class instead of via the class.
    #
    #           To be more precise, actually calls `.only_via_class` when called from an instance
    #           (this function normally throws the exception that it may not be accessed via an instance).
    #
    #   NOTE:
    #       When used with an instance of `Meta_Metaclass` (i.e.: the metaclass of all metaclasses) then the "class"
    #       is the metaclass (which is an instance of `Meta_Metaclass`), and the "instance" is the class (which is an
    #       instance of an instance of `Meta_Metaclass`).
    #
    #       Example:
    #
    #               Meta_Metaclass      --  Is its own metaclass
    #               MetaColor           --  Uses `Meta_Metaclass` as its metaclass.
    #               Color               --  Uses `MetaColor` at it's metaclass.
    #
    #               1.  Here class `Color` is an instance of metaclass `MetaColor`
    #
    #               2.  Likewise metaclass `Metacolor` is an instance of metaclass `Meta_Metaclass`
    #
    #               3.  Since `Meta_Metaclass` is it's own metaclass; then we can say that `Meta_Metaclass` is an
    #                   instance of `Meta_Metaclass`.
    #
    #       Given the above, then there will exist:
    #
    #               4.  Metacolor.create_class = StaticMethod_OnlyViaClass(...)
    #
    #       We can call this as follows (via the "class" -- which in this case is the metaclass `MetaColor`):
    #
    #               5.  Color = Metacolor.create_class('Color')
    #
    #       However, the following is illegal:
    #
    #               6.  AnotherColor = Color.create_class('AnotherColor')
    #
    #                   Here we are, illegally, calling `.create_class` via an instance (`Color`) of the
    #                   metaclass `Metacolor`.
    #
    #                   This will throw the following exception:
    #
    #           AttributeError: `.create_class` may only be called via the metaclass <Meta_Metaclass MetaColor>;
    #               not via the class <Metacolor Color>.
    #
    class StaticMethod_OnlyViaClass(Object):
        __slots__ = ((
            'f',                        #   Function
            'only_via_class',           #   Function
        ))


        def __get__(t, object, object_type):
            assert object_type.class_type is Meta_Metaclass

            if object is not none:
                t.only_via_class(object, object_type)

            return t.f


        def __repr__(t):
            return arrange('<StaticMethod_OnlyViaClass %s>', t.f.__name__)



    new__StaticMethod_OnlyViaClass                        = Method(new_instance, StaticMethod_OnlyViaClass)
    initialize__StaticMethod_OnlyViaClass__f              = StaticMethod_OnlyViaClass.f             .__set__
    initialize__StaticMethod_OnlyViaClass__only_via_class = StaticMethod_OnlyViaClass.only_via_class.__set__


    @share
    def create__StaticMethod_OnlyViaClass(f, only_via_class):
        r = new__StaticMethod_OnlyViaClass()

        initialize__StaticMethod_OnlyViaClass__f                       (r, f)
        initialize__StaticMethod_OnlyViaClass__only_via_class(r, only_via_class)

        return r
