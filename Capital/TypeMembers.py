#
#  Copyright (c) 2018 Joy Diamond.  All rights reserved.
#
@module('Capital.TypeMembers')
def module():
    show = 0


    require_module('Capital.Class_FindSymbol')


    #
    #   The base classes for a class derived from `Type`
    #
    base_classes__Type = ((Type,))


    #
    #   NOTE:
    #       See "LearningPython/TypeParts.py" for more details.
    #
    find_Type_member = produce__find_symbol(Type, show = show)


    #
    #   Access the members shared with `Object`
    #
   #Type__member__class_type         = find_Type_member('__class__')
   #Type__operator__delete_attribute = find_Type_member('__delattr__')
   #Type__member__documentation      = find_Type_member('__doc__')
   #Type__operator__format           = find_Type_member('__format__')
   #Type__operator__get_attribute    = find_Type_member('__getattribute__')
   #Type__operator__hash             = find_Type_member('__hash__')
    Type__operator__constructor      = find_Type_member('__init__')
   #Type__operator__new              = find_Type_member('__new__')
   #Type__operator__reduce           = find_Type_member('__reduce__')
   #Type__operator__reduce_extended  = find_Type_member('__reduce_ex__')
   #Type__operator__representation   = find_Type_member('__repr__')
   #Type__operator__set_attribute    = find_Type_member('__setattr__')
   #Type__operator__size_of          = find_Type_member('__sizeof__')
   #Type__operator__string           = find_Type_member('__str__')
   #Type__operator__subclass_hook    = find_Type_member('__subclasshook__')

   #if is_python_3:
   #    Type__operator__introspection = find_Type_member('__dir__')


    #
    #   Comparison operators
    #
   #Type__operator__equal                 = find_Type_member('__eq__')
   #Type__operator__greater_than          = find_Type_member('__gt__')
   #Type__operator__greater_than_or_equal = find_Type_member('__ge__')
   #Type__operator__less_than             = find_Type_member('__lt__')
   #Type__operator__less_than_or_equal    = find_Type_member('__le__')
   #Type__operator__not_equal             = find_Type_member('__ne__')


    #
    #   `Type` members *NOT* shared with `Object`
    #
   #Type__member__abstract_methods                        = find_Type_member('__abstractmethods__')
   #Type__member__class_parent                            = find_Type_member('__base__')
    Type__member__class_bases                             = find_Type_member('__bases__')
   #Type__member__class_basic_size                        = find_Type_member('__basicsize__')
    Type__operator__call                                  = find_Type_member('__call__')
    Type__member__class_members                           = find_Type_member('__dict__')
   #Type__member__class_members_offset                    = find_Type_member('__dictoffset__')
   #Type__member__class_flags                             = find_Type_member('__flags__')
   #Type__operator__instance_check                        = find_Type_member('__instancecheck__')
   #Type__member__class_item_size                         = find_Type_membe_('__itemsize__')
    Type__member__class_calculate_method_resolution_order = find_Type_member('mro')
    Type__member__method_resolution_order                 = find_Type_member('__mro__')
    Type__member__class_name                              = find_Type_member('__name__')
   #Type__operator__subclass_check                        = find_Type_member('__subclasscheck__')
   #Type__operator__subclasses                            = find_Type_member('__subclasses__')
   #Type__member__weak_reference_offset                   = find_Type_member('__weakrefoffset__')


    #
    #   create_python_type
    #
    #   NOTE:
    #       A python type is normally created as follows:
    #
    #           1.  M(name, bases, members).
    #
    #               where `M` is either `Type` or a derived type from `Type`
    #
    #       This means:
    #
    #           2.  type(M).__call__(M, name, bases, members)
    #
    #               [i.e.: Take the metaclass of `M` and call the special `.__call__` method]
    #
    #               Normally `.__call__` is not overriden and is inherited from `Type.__call__`.
    #
    #           3.  `Type.__call__` does the following:
    #
    #                R = M.__new__(name, bases, members)
    #
    #                if type(R) is M:
    #                    M.__init__(R, name, bases, members)
    #
    #                return R
    #
    #            (in other words the normal create a new instance procedure; call `.__new__` and if this
    #            returns the expected class [which is the case 99% of the time], then call `.__init__`)
    #
    #           4.  Nomally `M.__init__` is inherited from `Type.__init__`, and `Type.__init__` does nothing ...
    #
    #           5.  Thus for the cases when:
    #
    #                   A.  We want to use the default behavior of `Type.__call__`, `Type.__new__`, and `Type.__init__`
    #
    #               We can optimze as follows:
    #
    #                   1.  M(name, bases, members)                             #   See #1 above
    #
    #               to:
    #
    #                   2.  type(M).__call__(M, name, bases, members)           #   See #2 above
    #
    #               to:
    #
    #                   2B. Type.__call__(M, name, bases, members)              #   See #5A above
    #
    #               to:
    #
    #                   3.  Type.__new__(M, name, bases, members)
    #
    #                   [bypassing the calls to `type(M).__call__` and `M.__init__` since
    #                   they are optmized away]
    #
    #   This is all a long way of saying that `create_python_type` (an alias for `Type__operator__new;
    #   an alias for `Type.__new__`) is how to create new python types.
    #        
    create_python_type = find_Type_member('__new__')


    #
    #   python_class_name = Type::__name__::__get__
    #
    python_class_name = Type__member__class_name.__get__


    #
    #   python_class_members = Type::__dict__::__get__
    #
    python_class_members = Type__member__class_members.__get__


    share(
            'base_classes__Type',           base_classes__Type,

            'python_class_members',         python_class_members,
            'python_class_name',            python_class_name,

            'Type__member__class_bases',    Type__member__class_bases,

            'Type__member__class_calculate_method_resolution_order',
                Type__member__class_calculate_method_resolution_order,

            'Type__member__class_name',                 Type__member__class_name,
            'Type__member__method_resolution_order',    Type__member__method_resolution_order,
        )


    export(
            'create_python_type',           create_python_type,
            'Type__operator__call',         Type__operator__call,
            'Type__operator__constructor',  Type__operator__constructor,
        )
