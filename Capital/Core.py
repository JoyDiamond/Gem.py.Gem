#
#   Copyright (c) 2017-2018 Joy Diamond.  All rights reserved.
#
@module('Capital.Core')
def module():
    Python_Types = __import__('types')


    #
    #   Types
    #
    Method = Python_Types.MethodType


    #
    #   Functions
    #
    property    = Python_BuiltIn.property               #   Actually a type; but used as a function
    sorted_list = Python_BuiltIn.sorted



    #
    #   Local [modifiable] variables -- emulated, as not supported in python 2
    #
    local_variables = [
            0,                          #   position
            '',                         #   prefix
            '\n',                       #   prefix_blanks
            -1,                         #   total_blanks
        ]

    alter_variable = local_variables.__setitem__
    query_variable = local_variables.__getitem__

    alter__standard_output__position      = Method(alter_variable, 0)
    alter__standard_output__prefix        = Method(alter_variable, 1)
    alter__standard_output__prefix_blanks = Method(alter_variable, 2)
    alter__standard_output__total_blanks  = Method(alter_variable, 3)

    query__standard_output__position      = Method(query_variable, 0)
    query__standard_output__prefix        = Method(query_variable, 1)
    query__standard_output__prefix_blanks = Method(query_variable, 2)
    query__standard_output__total_blanks  = Method(query_variable, 3)

    alter__standard_output__position__0           = Method(alter__standard_output__position,     0)
    alter__standard_output__total_blanks__0       = Method(alter__standard_output__total_blanks, 0)
    alter__standard_output__total_blanks__1       = Method(alter__standard_output__total_blanks, 1)
    alter__standard_output__total_blanks__MINUS_1 = Method(alter__standard_output__total_blanks, -1)


    #
    #   execute
    #
    @export
    def execute(f):
        f()

        return execute


    #
    #   intern_arrange & intern_integer
    #
    @export
    def intern_arrange(format, *arguments):
        return intern_string(format % arguments)


    provide_integer = {}.setdefault


    @export
    def intern_integer(v):
        assert type(v) is Integer

        return provide_integer(v, v)


    #
    #   line & partial
    #
    flush_standard_output = Python_System.stdout.flush
    write_standard_output = Python_System.stdout.write


    @export
    def line(format = none, *arguments):
        if query__standard_output__position():
            #
            #   Finish current line (does not alter `total_blanks`)
            #
            alter__standard_output__position__0()

            if format is none:
                assert length(arguments) is 0

                write_standard_output('\n')
                flush_standard_output()
                return

            s = (format % arguments   if arguments else   format)

            write_standard_output(s + '\n')
            flush_standard_output()
            return

        total_blanks = query__standard_output__total_blanks()

        if format is none:
            assert length(arguments) is 0

            #
            #   Always write this [explicit] blank line out (even if blanks are currently disabled; or *IF* blanks
            #   are disabled in the future).
            #
            write_standard_output(query__standard_output__prefix_blanks())
            flush_standard_output()

            if total_blanks > 1:
                alter__standard_output__total_blanks(total_blanks - 1)

            return

        prefix = query__standard_output__prefix()
        s      = (format % arguments   if arguments else   format)

        if prefix is 0:
            if total_blanks > 0:
                write_standard_output(query__standard_output__prefix_blanks() * total_blanks + s + '\n')
            else:
                write_standard_output(s + '\n')
        else:
            if total_blanks > 0:
                write_standard_output(query__standard_output__prefix_blanks() * total_blanks + prefix + s + '\n')
            else:
                write_standard_output(prefix + s + '\n')

        #
        #   Always `total_blanks = 0`; that way even if was `-1` it is reset to `0`.
        #
        alter__standard_output__total_blanks__0()
        flush_standard_output()


    @export
    def partial(format, *arguments):
        s = (format % arguments   if arguments else   format)

        position = query__standard_output__position()

        total = position + length(s)

        if position > 0:
            #
            #   Finish current line (does not alter `total_blanks`)
            #
            write_standard_output(s)
        else:
            prefix       = query__standard_output__prefix()
            total_blanks = query__standard_output__total_blanks()

            if prefix is 0:
                if total_blanks > 0:
                    write_standard_output(query__standard_output__prefix_blanks() * total_blanks + s)
                else:
                    write_standard_output(s)
            else:
                total += length(prefix)

                if total_blanks > 0:
                    write_standard_output(query__standard_output__prefix_blanks() * total_blanks + prefix + s)
                else:
                    write_standard_output(prefix + s)

            alter__standard_output__total_blanks__0()

        alter__standard_output__position(total)

        flush_standard_output()


    #
    #   method_is_function
    #
    if is_python_2:
        @export
        @privileged
        def method_is_function(method, f):
            return method.im_func is f
    else:
        @export
        def method_is_function(method, f):
            return method is f


    #
    #   privileged_2
    #
    if is_python_2:
        export(
            'privileged_2',     rename_function('privileged_2', privileged)
        )
    else:
        @export
        def privileged_2(f):
            return f


    #
    #   property_3
    #
    @share
    def property_3(f):
        return property(f, f, f)



    #
    #   sorted_tuple
    #
    def sorted_tuple(iterable):
        return Tuple(sorted_list(iterable))


    share(
            #
            #   Local variables
            #
            'alter__standard_output__prefix',                   alter__standard_output__prefix,
            'alter__standard_output__prefix_blanks',            alter__standard_output__prefix_blanks,
            'alter__standard_output__total_blanks__1',          alter__standard_output__total_blanks__1,
            'alter__standard_output__total_blanks',             alter__standard_output__total_blanks,
            'alter__standard_output__total_blanks__MINUS_1',    alter__standard_output__total_blanks__MINUS_1,
            'query__standard_output__position',                 query__standard_output__position,
            'query__standard_output__prefix_blanks',            query__standard_output__prefix_blanks,
            'query__standard_output__prefix',                   query__standard_output__prefix,
            'query__standard_output__total_blanks',             query__standard_output__total_blanks,
        )


    export(
            #
            #   Types
            #
            #   NOTE:
            #       Do not use `Python_Types.ObjectType`, as this only exists in python 2 & *NOT* in python 3
            #
            'Boolean',      Python_BuiltIn.bool,
            'Bytes',        Python_BuiltIn.bytes,
            'Function',     execute.__class__,
            'FrozenSet',    Python_BuiltIn.frozenset,
            'Integer',      Python_BuiltIn.int,
            'LiquidSet',    Python_BuiltIn.set,
            'List',         Python_BuiltIn.list,
            'Long',         (Python_BuiltIn.long         if is_python_2 else   Python_BuiltIn.int),
            'Map',          Python_BuiltIn.dict,
            'MapProxy',     (Python_Types.DictProxyType  if is_python_2 else   Python_Types.MappingProxyType),
            'Method',       Method,
            'Object',       Python_BuiltIn.object,
            'Slice',        Python_BuiltIn.slice,
            'Tuple',        Python_BuiltIn.tuple,
            'Type',         Python_BuiltIn.type,


            #
            #   Functions
            #
            'address_of',       Python_BuiltIn.id,
            'attribute',        Python_BuiltIn.getattr,
            'character',        Python_BuiltIn.chr,
            'class_method',     Python_BuiltIn.classmethod,
            'enumerate',        Python_BuiltIn.enumerate,
            'globals',          Python_BuiltIn.globals,
            'hash',             Python_BuiltIn.hash,
            'is_instance',      Python_BuiltIn.isinstance,
            'is_subclass',      Python_BuiltIn.issubclass,
            'iterate',          Python_BuiltIn.iter,
            'iterate_range',    Python_BuiltIn.range,
            'maximum',          Python_BuiltIn.max,
            'minimum',          Python_BuiltIn.min,
            'ordinal',          Python_BuiltIn.ord,
            'portray',          Python_BuiltIn.repr,
            'property',         property,               #   Really a type, but treated as a function
            'set_attribute',    Python_BuiltIn.setattr,
            'sorted_list',      sorted_list,
            'sorted_tuple',     sorted_tuple,
            'static_method',    Python_BuiltIn.staticmethod,
            'sum',              Python_BuiltIn.sum,
            'type',             Python_BuiltIn.type,
        )


    if python_debug_mode:
        built_in(Python_Exception.AssertionError)
