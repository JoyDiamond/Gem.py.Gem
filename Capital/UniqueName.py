#
#   Copyright (c) 2017-2018 Joy Diamond.  All rights reserved.
#
@module('Capital.UniqueName')
def module():
    if is_python_2:
        require_module('Capital.System')                                #   MAXIMUM_INTEGER


    class UniqueName(Object):
        __slots__ = ((
            'prefix',                   #   String
            'value',                    #   Integer | Long
        ))


        def __init__(t, prefix):
            t.prefix = prefix
            t.value  = 0


        if is_python_2:
            def next(t):
                if t.value == MAXIMUM_INTEGER:
                    t.value = \
                        value = Long(MAXIMUM_INTEGER) + 1
                else:
                    t.value = \
                        value = t.value + 1

                return t.prefix + String(value)
        else:
            def next(t):
                t.value = \
                    value = t.value + 1

                return t.prefix + String(value)


        if is_python_2:
            def skip_value__ALLY__UnitTest(t, value):
                assert t.value < value

                t.value = value


    @export
    def create_UniqueName(prefix):
        return UniqueName(prefix)
